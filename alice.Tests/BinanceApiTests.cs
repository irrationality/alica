﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using alica.Core.CQRS.Binance;
using alica.Core.CQRS.Bittrex;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Xunit;

namespace alice.Tests
{
    public class BinanceApiTests
    {
        private readonly ConfigurationOptions _configuration;
        private readonly BinanceBalanceQuery _balanceQuery;
        private readonly BinanceCheckWalletQuery _checkWalletQuery;
        private readonly BinanceFilledOrderQuery _filledOrderQuery;
        private readonly BinanceMinBaseLotQuery _minBaseLotQuery;
        private readonly BinanceOrderCommand _orderCommand;
        private readonly BinanceWithdrawCommand _withdrawCommand;
        
        public BinanceApiTests()
        {
            var container = new ServiceContainer();
            var directory = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin", StringComparison.Ordinal));
            var config = new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile("settings.json").Build();
            _configuration = new ConfigurationOptions()
            {
                BittrexApiBasicUrl = config["StockAPIs:Bittrex"],
                HitBtcApiBasicUrl = config["StockAPIs:HitBtc"],
                YobitApiBasicUrl = config["StockAPIs:Yobit"],
                YobitApiTradeUrl = config["StockAPIs:YobitPrivate"],
                BittrexApiKey = config["ApiKeys:Bittrex"],
                HitBtcApiKey = config["ApiKeys:HitBtc"],
                BittrexSecret = config["ApiKeys:BitSecret"],
                BinanceApiKey = config["ApiKeys:Binance"],
                BinanceSecret = config["ApiKeys:BinanceSecret"],
                YobitApiKey = config["ApiKeys:Yobit"],
                YobitSecret = config["ApiKeys:YobitSecret"],
                Wallets = config.GetSection("Wallets").Get<Dictionary<StockType, List<Wallet>>>()
            };
            IOptions<ConfigurationOptions> options = new OptionsWrapper<ConfigurationOptions>(_configuration);
            container.AddService(typeof(IOptions<ConfigurationOptions>), options);
            AlicaServiceLocator.Provider = container;
            _balanceQuery = new BinanceBalanceQuery();
            _checkWalletQuery = new BinanceCheckWalletQuery();
            _filledOrderQuery = new BinanceFilledOrderQuery();
            _minBaseLotQuery = new BinanceMinBaseLotQuery();
            _orderCommand = new BinanceOrderCommand();
            _withdrawCommand = new BinanceWithdrawCommand();
        }

        [Fact]
        public void GetBalance_Successful()
        {
            var result = _balanceQuery.ExecuteAsync("eth").Result;
            Assert.True(result.Available > 0.0m);
        }

        [Fact]
        public void CheckWallet_Successful()
         {
            var result = _checkWalletQuery.QueryAsync("btc");
            Assert.True(result.Result.Deposit);
        }

        [Fact]
        public void GetMinBaseLot_Successful()
        {
            var result = _minBaseLotQuery.ExecuteAsync("BTC", "ETH");
            Assert.True(result.Result != 0.0m);
        }

//        [Fact]
//        public void CreateOrderSuccessful()
//        {
//            var baseCurrency = "ETH";
//            var quoteCurrency = "BTC";
//            var quantity = 0.0028335m;
//            var result = _orderCommand.CreateAsync(baseCurrency, quoteCurrency, quantity, 0.2m, SideType.Sell);
//            Assert.NotNull(result.Result);
//            Assert.False(string.IsNullOrEmpty(result.Result));
//        }
    }
}