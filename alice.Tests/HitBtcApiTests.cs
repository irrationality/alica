﻿using System;
using System.Collections.Generic;
using alica.Core.CQRS.HitBtc;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Microsoft.Extensions.Configuration;
using Xunit;


namespace alice.Tests
{
    public class HitBtcApiTests
    {
        private readonly ConfigurationOptions _configuration;
        private readonly HitBalanceQuery _balanceQuery;
        private readonly HitCheckWalletQuery _checkWalletQuery;
        private readonly HitFilledOrderQuery _filledOrderQuery;
        private readonly HitMinBaseLotQuery _minBaseLotQuery;
        private readonly HitOrderCommand _orderCommand;
        private readonly HitTickSizeQuery _tickSizeQuery;
        private readonly HitWithdrawCommand _withdrawCommand;
        
        public HitBtcApiTests()
        {
            var directory = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin", StringComparison.Ordinal));
            var config = new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile("settings.json").Build();
            _configuration = new ConfigurationOptions()
            {
                BittrexApiBasicUrl = config["StockAPIs:Bittrex"],
                HitBtcApiBasicUrl = config["StockAPIs:HitBtc"],
                BittrexApiKey = config["ApiKeys:Bittrex"],
                HitBtcApiKey = config["ApiKeys:HitBtc"],
                Wallets = config.GetSection("Wallets").Get<Dictionary<StockType, List<Wallet>>>()
            };
            _balanceQuery = new HitBalanceQuery();
            _checkWalletQuery = new HitCheckWalletQuery();
            _filledOrderQuery = new HitFilledOrderQuery();
            _minBaseLotQuery = new HitMinBaseLotQuery();
            _orderCommand = new HitOrderCommand();
            _tickSizeQuery = new HitTickSizeQuery();
            _withdrawCommand = new HitWithdrawCommand();
        }

        [Fact]
        public void GetBalance_Successful()
        {
            var result = _balanceQuery.ExecuteAsync();
            Assert.NotEmpty(result.Result);
        }

        [Fact]
        public void CheckWallet_Successful()
        {
            var result = _checkWalletQuery.ExecuteAsync("btc");
            Assert.True(result.Result.Deposit);
        }

        [Fact]
        public void GetMinBaseLot_Successful()
        {
            var result = _minBaseLotQuery.ExecuteAsync("ETHBTC").Result;
            Assert.True(result != 0.0m);
        }

        [Fact]
        public void GetTickSizeCollection_Successful()
        {
//            var result = _tickSizeQuery.ExecuteAsync();
//            Assert.NotEmpty(result.Result);
        }        
    }
}