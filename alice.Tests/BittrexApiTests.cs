﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using alica.Core.CQRS.Bittrex;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Xunit;

namespace alice.Tests
{
    public class BittrexApiTests
    {
        private readonly ConfigurationOptions _configuration;
        private readonly BitBalanceQuery _balanceQuery;
        private readonly BitCheckWalletQuery _checkWalletQuery;
        private readonly BitFilledOrderQuery _filledOrderQuery;
        private readonly BitMinBaseLotQuery _minBaseLotQuery;
        private readonly BitOrderCommand _orderCommand;
        private readonly BitTickSizeQuery _tickSizeQuery;
        private readonly BitWithdrawCommand _withdrawCommand;
        
        public BittrexApiTests()
        {
            var container = new ServiceContainer();
            var directory = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin", StringComparison.Ordinal));
            var config = new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile("settings.json").Build();
            _configuration = new ConfigurationOptions()
            {
                BittrexApiBasicUrl = config["StockAPIs:Bittrex"],
                HitBtcApiBasicUrl = config["StockAPIs:HitBtc"],
                BittrexApiKey = config["ApiKeys:Bittrex"],
                HitBtcApiKey = config["ApiKeys:HitBtc"],
                BittrexSecret = config["ApiKeys:BitSecret"],
                Wallets = config.GetSection("Wallets").Get<Dictionary<StockType, List<Wallet>>>()
            };
            IOptions<ConfigurationOptions> options = new OptionsWrapper<ConfigurationOptions>(_configuration);
            container.AddService(typeof(IOptions<ConfigurationOptions>), options);
            AlicaServiceLocator.Provider = container;
            _balanceQuery = new BitBalanceQuery();
            _checkWalletQuery = new BitCheckWalletQuery();
            _filledOrderQuery = new BitFilledOrderQuery();
            _minBaseLotQuery = new BitMinBaseLotQuery();
            _orderCommand = new BitOrderCommand();
            _tickSizeQuery = new BitTickSizeQuery();
            _withdrawCommand = new BitWithdrawCommand();
        }

        [Fact]
        public void GetBalance_Successful()
        {
            var result = _balanceQuery.ExecuteAsync("eth").Result;
            Assert.True(result.Available > 0.0m);
        }

        [Fact]
        public void CheckWallet_Successful()
         {
            var result = _checkWalletQuery.QueryAsync("btc");
            Assert.True(result.Result.Deposit);
        }

        [Fact]
        public void GetMinBaseLot_Successful()
        {
           // var result = _minBaseLotQuery.ExecuteAsync("BTC-ETH");
          //  Assert.True(result.Result != 0.0m);
        }

        [Fact]
        public void GetTickSizeCollection_Successful()
        {
            var result = _tickSizeQuery.AllSizesQueryAsync();
            Assert.NotEmpty(result.Result);
        }


        [Fact]
        public void CheckTickSizeScaling_Successful()
        {
            var input = 0.00005m;
            var expectedResult = 0.00001m;
            var result = _tickSizeQuery.GetScaledOne(input);
            Assert.Equal(expectedResult, result);
            var input2 = 0.02m;
            var expectedResult2 = 0.01m;
            var result2 = _tickSizeQuery.GetScaledOne(input2);
            Assert.Equal(expectedResult2, result2);
        }
        
//        [Fact]
//        public void CreateOrderSuccessful()
//        {
//            var baseCurrency = "ETH";
//            var quoteCurrency = "BTC";
//            var quantity = 0.0028335m;
//            var result = _orderCommand.CreateAsync(baseCurrency, quoteCurrency, quantity, 0.2m, SideType.Sell);
//            Assert.NotNull(result.Result);
//            Assert.False(string.IsNullOrEmpty(result.Result));
//        }
    }
}