﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using alica.Core.CQRS;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Xunit;

namespace alice.Tests
{
    public class PairsTest
    {
        private readonly CheckCurrencyQuery _checkCurrencyQuery;
        public PairsTest()
        {
            var container = new ServiceContainer();
            var directory = AppContext.BaseDirectory.Substring(0, AppContext.BaseDirectory.IndexOf("bin", StringComparison.Ordinal));
            var config = new ConfigurationBuilder()
                .SetBasePath(directory)
                .AddJsonFile("settings.json").Build();
            var configuration = new ConfigurationOptions()
            {
                BittrexApiBasicUrl = config["StockAPIs:Bittrex"],
                HitBtcApiBasicUrl = config["StockAPIs:HitBtc"],
                BittrexApiKey = config["ApiKeys:Bittrex"],
                HitBtcApiKey = config["ApiKeys:HitBtc"],
                Wallets = config.GetSection("Wallets").Get<Dictionary<StockType, List<Wallet>>>()
            };
            _checkCurrencyQuery = new CheckCurrencyQuery();
            IOptions<ConfigurationOptions> options = new OptionsWrapper<ConfigurationOptions>(configuration);
            container.AddService(typeof(IOptions<ConfigurationOptions>), options);
            AlicaServiceLocator.Provider = container;
        }

        [Fact]
        public void CheckPairs_Successful()
        {
            var bitBase = "BTC";
            var bitQuote = "ETH";
            var hitBase = "ETH";
            var hitQuote = "BTC";
            var result = _checkCurrencyQuery.QueryAsync(hitBase, hitQuote, bitBase, bitQuote).Result;
            Assert.True(result);
            var wrongBase = "KEK";
            var wrongQuote = "PEK";
            var wrongResult = _checkCurrencyQuery.QueryAsync(wrongBase, wrongQuote, wrongBase, wrongQuote).Result;
            Assert.False(wrongResult);
        }
    }
}