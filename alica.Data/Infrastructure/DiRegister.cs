﻿using System;

namespace alica.Data.Infrastructure
{

    public class RegisterToDiAttribute : Attribute
    {
        public Type InterfaceType { get; }

        public RegisterToDiAttribute(Type interfaceType)
        {
            InterfaceType = interfaceType;
        }

        public RegisterToDiAttribute()
        {
            
        }
    }
}