﻿using System.Collections.Generic;
using alica.Data.Models;

namespace alica.Data.Infrastructure
{
    public class ConfigurationOptions
    {
        public string HitBtcApiBasicUrl { get; set; }
        public string BittrexApiBasicUrl { get; set; }
        public string BittrexApiKey { get; set; }
        public string BittrexSecret { get; set; }
        public string HitBtcApiKey { get; set; }
        public string BinanceApiKey { get; set; }
        public string BinanceSecret { get; set; }
        public string YobitApiBasicUrl { get; set; }
        public string YobitApiTradeUrl { get; set; }
        public string YobitApiKey { get; set; }
        public string YobitSecret { get; set; }
        
        public Dictionary<StockType, List<Wallet>> Wallets { get; set; }
    }

    public class Wallet
    {
        public string Currency { get; set; }
        public string Address { get; set; }
    }
}
/*

"Logging": {
"IncludeScopes": false,
"LogLevel": {
"Default": "Warning"
}
},
"StockAPIs":{
"Bittrex":"https://bittrex.com/api/v1.1",
"HitBtc":"https://api.hitbtc.com/api/2"
},
"ApiKeys": {
"Bittrex":"Not filled",
"HitBtc":"Not filled"
*/
