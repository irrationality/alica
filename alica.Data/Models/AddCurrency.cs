﻿namespace alica.Data.Models
{
    public class AddCurrency
    {
        public string HitBase { get; }
        public string BitBase { get; }
        public string HitQuote { get; }
        public string BitQuote { get; }


        public string Pair => $"{HitBase}{HitQuote}_{BitBase}{BitQuote}";
        public decimal TickSize { get;  }

        public AddCurrency(string hitBase, string hitQuote ,string bitBase, string bitQuote, decimal tickSize)
        {
            HitBase = hitBase;
            BitBase = bitBase;
            HitQuote = hitQuote;
            BitQuote = bitQuote;
            TickSize = tickSize;
        }
    }
}