﻿namespace alica.Data.Models
{
    public class GetBalanceResult
    {
        #region Properties

        public string Currency { get; }
        public decimal Available { get; }
        public decimal Reserved { get; }

        #endregion

        #region Constructors

        public GetBalanceResult(string curr, decimal available, decimal reserved)
        {
            Currency = curr;
            Available = available;
            Reserved = reserved;
        }

        #endregion
    }
}