﻿namespace alica.Data.Models
{
    public class RemoveCurrency
    {
        public string HitBase { get; }
        public string BitBase { get; }
        public string HitQuote { get; }
        public string BitQuote { get; }
        
        public string Pair => $"{HitBase}{HitQuote}_{BitBase}{BitQuote}";

        
        public RemoveCurrency(string hitBase, string bitBase, string hitQuote, string bitQuote)
        {
            HitBase = hitBase;
            BitBase = bitBase;
            HitQuote = hitQuote;
            BitQuote = bitQuote;
        }
    }
}