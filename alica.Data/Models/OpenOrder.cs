﻿namespace alica.Data.Models
{
    public class OpenOrder
    {
        public decimal Qty { get; set; }
        public decimal Rate { get; set; }
        public StockType StockType { get; set; }
        public string Id { get; set; }
        public bool IsSuceed { get; set; }
        public SideType SideType { get; set; }
    }
}