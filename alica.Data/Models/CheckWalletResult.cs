﻿namespace alica.Data.Models
{
    public class CheckWalletResult
    {
        public CheckWalletResult(string currency, bool deposit, bool withdraw)
        {
            Currency = currency;
            Deposit = deposit;
            Withdraw = withdraw;
        }

        public string Currency { get;  }
        public bool Deposit { get;  }
        public bool Withdraw { get;  }
    }
}