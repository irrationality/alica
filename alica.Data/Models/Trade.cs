﻿namespace alica.Data.Models
{
    public class Trade
    {
        public Trade()
        {
            
        }
        public Trade(Order bid, Order ask, decimal spread)
        {
            Bid = bid;
            Ask = ask;
            Spread = spread;
        }

        public Order Bid { get; }
        public Order Ask { get;  }
        public decimal Spread { get; }
    }
}