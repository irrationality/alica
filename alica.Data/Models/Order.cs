﻿namespace alica.Data.Models
{
    public class Order
    {
        public Order(OrderType type, StockType stock, decimal qty, decimal rate, string currencyPair)
        {
            Type = type;
            Stock = stock;
            Qty = qty;
            Rate = rate;
            CurrencyPair = currencyPair;
        }
        public OrderType Type { get;  }
        public StockType Stock { get;  }
        public decimal Qty { get;  }
        public decimal Rate { get; }
        public string CurrencyPair { get; }
    }
}