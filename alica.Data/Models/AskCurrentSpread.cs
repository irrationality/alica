﻿namespace alica.Data.Models
{
    public class AskCurrentSpread
    {
       
        public StockType StockType { get;  }
        public decimal Rate { get;  }
        public SideType SideType { get; }

        public AskCurrentSpread(StockType type, decimal rate, SideType sideType)
        {
            StockType = type;
            Rate = rate;
            SideType = sideType;
        }
    }
}