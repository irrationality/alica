﻿namespace alica.Data.Models
{
    public enum StockType
    {
        HitBtc = 1,
        Bittrex = 2,
        Binance = 3,
        Yobit = 4
    }
}