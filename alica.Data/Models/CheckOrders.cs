﻿namespace alica.Data.Models
{
    public class CheckOrders
    {
        public decimal Lot { get; }
        public Trade Trade { get; }

        public CheckOrders(decimal lot, Trade trade)
        {
            Lot = lot;
            Trade = trade;
        }
    }
}