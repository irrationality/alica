﻿namespace alica.Data.Models
{
    public class ChangeTradingMode
    {
        public TradingMode Mode { get; private set; }
        public int DeltaPercent { get; private set; }

        public ChangeTradingMode(TradingMode mode, int deltaPercent)
        {
            Mode = mode;
            DeltaPercent = deltaPercent;
        }
    }

    public enum TradingMode
    {
        Trade,
        Watch
    }
}