﻿namespace alica.Data.Models
{
    public class TickSize
    {
        #region Properties

        
        public string BaseCurrency { get; }
        public string QuoteCurrency { get; }
        public decimal Size { get; }
        public StockType Stock { get;  }

        #endregion

        #region Constructors

        public TickSize(string baseCurrency, string quoteCurrency, decimal size, StockType stock)
        {
            BaseCurrency = baseCurrency;
            QuoteCurrency = quoteCurrency;
            Size = size;
            Stock = stock;
        }

        #endregion
    }
}