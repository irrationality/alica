﻿namespace alica.Data.Models
{
    public class OrderBooks
    {
        public Order[] HitAsk { get; }
        public Order[] HitBid { get; }
        public Order[] BittAsk { get; }
        public Order[] BittBid { get; }

        public OrderBooks(Order[]hitAsk, Order[]hitBid, Order[] bittAsk, Order[] bittBid)
        {
            HitAsk = hitAsk;
            HitBid = hitBid;
            BittAsk = bittAsk;
            BittBid = bittBid;
        }
    }

    public class OrderBook
    {
        public Order[] Bids { get; }
        public Order[] Asks { get; }

        public OrderBook(Order[] bids, Order[] asks)
        {
            Bids = bids;
            Asks = asks;
        }
            
    }
    
}