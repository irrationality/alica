﻿using System;
using alica.Data.Models;
using Dapper;
using Npgsql;

namespace alica.Data.CQRS
{
    public class WriteTradeToDbCommand : IDisposable
    {
        private const string __sql = 
            @"INSERT INTO trades.trade(
                bid_rate, 
                bid_qty, 
                bid_stock, 
                ask_rate, 
                ask_qty, 
                ask_stock, 
                trade_date,
                currency_pair)
            VALUES (
                @bidRate, 
                @bidQty, 
                @bidStock, 
                @askRate, 
                @askQty, 
                @askStock, 
                @tradeDate, 
                @currencyPair);";

        private const string _sql = 
            @"INSERT INTO trades.trade_emulation(
                lot, 
                buy_rate, 
                sell_rate, 
                bit_base_wallet, 
                bit_quote_wallet, 
                hit_base_wallet, 
                hit_quote_wallet, 
                currency_pair, 
                date)
            VALUES (
                @lot,
                @buyRate,
                @sellRate,
                @bitBaseWallet,
                @bitQuoteWallet,
                @hitBaseWallet,
                @hitQuoteWallet,
                @currencyPair,
                @date);";
        
        private readonly NpgsqlConnection _connection;
        
        
        public WriteTradeToDbCommand()
        {
            _connection = new NpgsqlConnection("Host=82.146.32.204;Port=5432;Username=alica;Password=Jihadkavkaz1492!Alica;Database=alica;Pooling=True;CommandTimeout=900;");
        }

        public void Execute(decimal lot,decimal buyRate, decimal sellRate, decimal bitBaseWallet, 
            decimal bitQuoteWallet, decimal hitBaseWallet, decimal hitQuoteWallet, string currencyPair)
        {
            _connection.Execute(_sql, new
            {
                lot,
                buyRate,
                sellRate,
                bitBaseWallet,
                bitQuoteWallet,
                hitBaseWallet,
                hitQuoteWallet,
                currencyPair,
                date = DateTime.Now
            });
        }
        
        public void ExecuteTrade(Trade trade)
        {
            _connection.Execute(__sql, new
            {
                bidRate = trade.Bid.Rate,
                bidQty = trade.Bid.Qty,
                bidStock = trade.Bid.Stock,
                askRate = trade.Ask.Rate,
                askQty = trade.Ask.Qty,
                askStock = trade.Ask.Stock,
                tradeDate = DateTime.Now,
                currencyPair = trade.Ask.CurrencyPair
            });
        }

        public void Dispose()
        {
            _connection?.Dispose();
        }
    }
}