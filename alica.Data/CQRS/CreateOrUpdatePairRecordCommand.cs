﻿using System;
using alica.Data.Infrastructure;
using Dapper;
using Npgsql;

namespace alica.Data.CQRS
{
    [RegisterToDi]
    public class CreateOrUpdatePairRecordCommand : IDisposable
    {
        private const string _sql =
            @"INSERT INTO infrastructure.currency_pairs(
                hit_base, 
                hit_quote, 
                bit_base, 
                bit_quote, 
                tick_size, 
                pair_code)
            VALUES (
                @hitBase, 
                @hitQuote, 
                @bitBase, 
                @bitQuote, 
                @tickSize, 
                @pairCode)
            ON CONFLICT ON CONSTRAINT currency_pairs_pair_code_key DO NOTHING";

        private readonly NpgsqlConnection _connection;


        public CreateOrUpdatePairRecordCommand()
        {
            _connection =
                new NpgsqlConnection(
                    "Host=82.146.32.204;Port=5432;Username=alica;Password=Jihadkavkaz1492!Alica;Database=alica;Pooling=True;CommandTimeout=900;");
        }

        public void Execute(string hitBase, string hitQuote, string bitBase, string bitQuote, decimal tickSize)
        {
            _connection.Execute(_sql, new
            {
                hitBase,
                hitQuote,
                bitBase,
                bitQuote,
                tickSize,
                pairCode = $"{hitBase}_{hitQuote}_{bitBase}_{bitQuote}"
            });
        }


        public void Dispose()
        {
            _connection?.Close();
            _connection?.Dispose();
        }
    }
}