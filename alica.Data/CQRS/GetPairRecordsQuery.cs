﻿using System;
using System.Collections.Generic;
using System.Linq;
using alica.Data.Infrastructure;
using Dapper;
using Npgsql;

namespace alica.Data.CQRS
{
    [RegisterToDi]
    public class GetPairRecordsQuery: IDisposable
    {
        private const string _sql =
            @"SELECT 
                hit_base, 
                hit_quote, 
                bit_base, 
                bit_quote, 
                tick_size FROM  infrastructure.currency_pairs"; 

        private readonly NpgsqlConnection _connection;


        public GetPairRecordsQuery()
        {
            _connection =
                new NpgsqlConnection(
                    "Host=82.146.32.204;Port=5432;Username=alica;Password=Jihadkavkaz1492!Alica;Database=alica;Pooling=True;CommandTimeout=900;");
        }

        public List<(string hitBase, string hitQuote, string bitBase, string bitQuote, Decimal tickSize )> Execute()
        {
            var qq = _connection.Query(_sql)
                .Select(q => ((string) q.hit_base,(string)q.hit_quote, 
                    (string) q.bit_base,(string)q.bit_quote, (decimal) q.tick_size  )).ToList();
            return qq;
        }


        public void Dispose()
        {
            _connection?.Close();
            _connection?.Dispose();
        }
    }
}