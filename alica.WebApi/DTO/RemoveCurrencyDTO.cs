﻿namespace alica.DTO
{
    public class RemoveCurrencyDTO
    {
        public string HitBase { get; set; }
        public string HitQuote { get; set; }
        public string BitBase { get; set; }
        public string BitQuote { get; set; }
    }
}