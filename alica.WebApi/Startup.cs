using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using alica.Core;
using alica.Core.CQRS.Bittrex;
using alica.Core.CQRS.HitBtc;
using alica.Data.CQRS;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Akka.Actor;
using Akka.Streams;
using Akka.Util.Internal;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Sockets;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Formatting.Json;


namespace alica
{
    public class Startup
    {
        private IActorRef supervisor;
        private ActorSystem actorSystem;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var system = ActorSystem.Create("actorSystem");
            supervisor = system.ActorOf(Props.Create(() => new Supervisor()), "supervisor");
            system.ActorOf(Props.Create(() => new SpreadPublisherActor()), "spreadpublisher");
            
            using (var client = new HttpClient())
            {
                while (true)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                    var resp = client.GetAsync("https://yobit.net/api/3/depth/btc_usd?limit=50").Result;
                    var str = Encoding.UTF8.GetString(((MemoryStream)resp.Content.ReadAsStreamAsync().Result).ToArray());
                    Console.WriteLine(str.Substring(0,100));
                }
            }

            using (var materializer = system.Materializer())
            {
                var qq = new OrderBookStream(materializer, "btc", "usd", "btc", "usdt");
                while (true)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(2));
                }
            }

            services.AddMvc();
            services.AddSignalR();
            services.AddSingleton(system);
            services.AddCors(options => options.AddPolicy("AllowCors",
                builder =>
                {
                    builder
                        .AllowAnyOrigin()
                        .AllowCredentials()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                }));

            var assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(a => a.FullName.Contains("alica")).ToList();
            var registrations = assemblies.SelectMany(a1 => a1.GetExportedTypes()
                .SelectMany(t => t.GetCustomAttributes(false).Where(a => a is RegisterToDiAttribute)
                    .Select(a => (t, (a as RegisterToDiAttribute).InterfaceType))));

            foreach (var (type, interfaceType) in registrations)
            {
                if (interfaceType == default)
                    services.AddTransient(type);
                else
                    services.AddTransient(interfaceType, type);
            }

            system.Scheduler.ScheduleTellRepeatedly(
                TimeSpan.FromMilliseconds(1000),
                TimeSpan.FromMilliseconds(1000),
                supervisor,
                new CallBittrex(),
                ActorRefs.NoSender);

            services.AddSingleton(system);
            services.Configure<ConfigurationOptions>(options =>
            {
                options.BittrexApiBasicUrl = Configuration["StockAPIs:Bittrex"];
                options.HitBtcApiBasicUrl = Configuration["StockAPIs:HitBtc"];
                options.YobitApiBasicUrl = Configuration["StockAPIs:Yobit"];
                options.YobitApiTradeUrl = Configuration["StockAPIs:YobitPrivate"];
                options.BittrexApiKey = Configuration["ApiKeys:Bittrex"];
                options.HitBtcApiKey = Configuration["ApiKeys:HitBtc"];
                options.BittrexSecret = Configuration["ApiKeys:BitSecret"];
                options.BinanceApiKey = Configuration["ApiKeys:Binance"];
                options.BinanceSecret = Configuration["ApiKeys:BinanceSecret"];
                options.YobitApiKey = Configuration["ApiKeys:Yobit"];
                options.YobitSecret = Configuration["ApiKeys:YobitSecret"];
                options.Wallets = Configuration.GetSection("Wallets").Get<Dictionary<StockType, List<Wallet>>>();
            });

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console(new JsonFormatter())
                .WriteTo.File(new JsonFormatter(), "log.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMvc();
            app.UseCors("AllowCors");
            app.UseSignalR(routes => { routes.MapHub<SpreadPublisher>("spread"); });
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/build"))
            });

            AlicaServiceLocator.Provider = app.ApplicationServices;
            var qq = AlicaServiceLocator.GetService<GetPairRecordsQuery>().Execute();
            //  qq.ForEach(q => supervisor.Tell(new AddCurrency(q.hitBase,q.hitQuote, q.bitBase,q.bitQuote,q.tickSize)));           
            //  supervisor.Tell(new AddCurrency("etc", "btc", "etc", "btc", 0.0000001m));
        }
    }
}