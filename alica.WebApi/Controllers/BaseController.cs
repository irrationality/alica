﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace alica.Controllers
{
    public class BaseController : Controller
    {
        private static IServiceProvider _serviceProvider;
        public static IServiceProvider ServiceProvider
        {
            protected get => _serviceProvider;
            set
            {
                if (_serviceProvider != default(IServiceProvider))
                    throw new Exception("ServiceProvider can be initialized at once");
                _serviceProvider = value;
            }
        }

        public IActionResult Invoke(Func<IActionResult> func)
        {
            try
            {
                return func();
            }
            catch (Exception ex)
            {
                var json = JsonConvert
                    .SerializeObject(
                        new
                        {
                            error = ex.Message,
                            exStackTrace = ex.StackTrace
                        });
                return new BadRequestObjectResult(json);
            }
        }

        public async Task<IActionResult> Invoke(Func<Task<IActionResult>> func)
        {
            try
            {
                return await func();
            }
            catch (Exception ex)
            {
                var json = JsonConvert
                    .SerializeObject(
                        new
                        {
                            error = ex.Message,
                            exStackTrace = ex.StackTrace
                        });
                return new BadRequestObjectResult(json);
            }
        }


        public async Task<IActionResult> InvokeCached(Func<Task<IActionResult>> func, string key)
        {
            try
            {
                var _cache = ServiceProvider.GetService<IMemoryCache>();
                if (_cache.TryGetValue(key, out IActionResult res)) return res;
                
                res = await func();
                _cache.Set(key, res, TimeSpan.FromMinutes(15));
                return res;
            }
            catch (Exception ex)
            {
                var json = JsonConvert
                    .SerializeObject(
                        new
                        {
                            error = ex.Message,
                            exStackTrace = ex.StackTrace
                        });
                return new BadRequestObjectResult(json);
            }
        }
    }
}