﻿using alica.Data.Models;
using Akka.Actor;
using Microsoft.AspNetCore.Mvc;

namespace alica.Controllers
{
    public class TradeController : BaseController
    {
        [HttpPost]
        [Route("api/[controller]/switchmode")]
        public IActionResult SwitchMode(TradingMode mode, int deltaPercent,
            [FromServices] ActorSystem actorSystem)
            => Invoke(() =>
            {
                var processActor = actorSystem.ActorSelection("/user/supervisor/process");
                processActor.Tell(new ChangeTradingMode(mode, deltaPercent));
                return new StatusCodeResult(200);
            });
    }
}