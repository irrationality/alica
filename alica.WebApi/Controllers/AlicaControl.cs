﻿using System.Threading.Tasks;
using alica.Core.CQRS;
using alica.Data.Models;
using alica.DTO;
using Akka.Actor;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace alica.Controllers
{
    [EnableCors("AllowCors")]
    public class AlicaControlController : BaseController
    {

        [HttpPost]
        [Route("api/[controller]/addcurrency")]
        public IActionResult AddCurrency([FromBody] AddCurrencyDTO model,
            [FromServices] ActorSystem actorSystem)
            => Invoke(() =>
            {
                var supervisor = actorSystem.ActorSelection("/user/supervisor");
                supervisor.Tell(new AddCurrency(
                    model.HitBase.ToLower(), 
                    model.HitQuote.ToLower(),
                    model.BitBase.ToLower(), 
                    model.BitQuote.ToLower(), 
                    model.TickSize));
                supervisor.Tell(new CallBittrex());
                return new StatusCodeResult(200);
            });
        
        
        [HttpPost]
        [Route("api/[controller]/removecurrency")]
        public IActionResult RemoveCurrency([FromBody]RemoveCurrencyDTO model,
            [FromServices] ActorSystem actorSystem)
            => Invoke(() =>
            {
                var supervisor = actorSystem.ActorSelection("/user/supervisor");
                supervisor.Tell(new RemoveCurrency(model.HitBase, model.BitBase, model.HitQuote,
                    model.BitQuote));
                return new StatusCodeResult(200);
            });
        
        
        [HttpGet]
        [Route("api/[controller]/checkcurrency")]
        public Task<IActionResult> СheckCurrency(
            string hitBase, string hitQuote, string bitBase, string bitQuote, 
            [FromServices] CheckCurrencyQuery checkCurrency)
            => Invoke(async () =>
            {
                var result  = await checkCurrency.QueryAsync(hitBase,hitQuote, bitBase,bitQuote);
                return new OkObjectResult(result);
            });
    }
}