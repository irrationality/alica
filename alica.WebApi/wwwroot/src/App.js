import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import Spreads from './Scenes/Spreads/Spreads';
import { ToastContainer } from 'react-toastify';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Alica</h1>
        </header>
        <Spreads/>
        <ToastContainer/>  
      </div>
    );
  }
}

export default App;
