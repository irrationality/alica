import React, { Component } from 'react';
import './Spreads.css';
import ReactDataGrid from 'react-data-grid';
import { HubConnection } from '@aspnet/signalr-client';
import {defaultUrl, stockTypes} from "../../constants";
import NewPair from "./Components/NewPair";
import {postData} from "../../utils";
import { toast } from 'react-toastify';


class Spreads extends Component {
    constructor(props){
        super(props);
        this._columns = [
            { key: 'Spread', name: 'Spread (%)' },
            { key: 'BuyStock', name: 'Buy Stock' },
            { key: 'SellStock', name: 'Sell Stock' },
            { key: 'BuyPair', name: 'Buy Pair' },
            { key: 'SellPair', name: 'Sell Pair' },
            { key: 'SellQty', name: 'Sell Quantity' },
            { key: 'SellRate', name: 'Sell Rate' },
            { key: 'BuyQty', name: 'Buy Quantity' },
            { key: 'BuyRate', name: 'Buy Rate' },
            { key:'$delete', name:'Actions', getRowMetaData:(row) => row,
                formatter: ({dependentValues}) =>
                    (<span><button type="button" onClick={() => this.deleteSpread(dependentValues)} className="btn btn-danger">X</button></span>)}
                ];
        this._rows = [];
        this.state = {
            hubConnection: null,
            spreads: [],
            modalOpened: false
        };
    }
    
    deleteSpread = (spreadObj) => {
        let removeIndex = this.state.spreads.findIndex(el => el.BuyPair === spreadObj.BuyPair
            && el.SellPair === spreadObj.SellPair);
        if (removeIndex < 0) return;
        let spread = this.state.spreads[removeIndex];
        postData(defaultUrl+'/api/AlicaControl/removecurrency', {
            hitBase: spread.BuyStock === 'HitBTC' ? spread.BuyPair.split('_')[0] : spread.SellPair.split('_')[0],
            hitQuote: spread.BuyStock === 'HitBTC' ? spread.BuyPair.split('_')[1] : spread.SellPair.split('_')[1],
            bitBase: spread.BuyStock === 'Bittrex' ? spread.BuyPair.split('_')[0] : spread.SellPair.split('_')[0],
            bitQuote: spread.BuyStock === 'Bittrex' ? spread.BuyPair.split('_')[1] : spread.SellPair.split('_')[1]
        }).then(()=>{
            let spreads = this.state.spreads.filter(el => el.BuyPair !== spreadObj.BuyPair
                && el.SellPair !== spreadObj.SellPair);
            this._rows = spreads;
            this.setState({spreads});
            toast.success("Deleted successfully");
        }).catch((error)=>toast.error(error.message));
    };
    
    componentDidMount = () => {
        const hubConnection = new HubConnection(defaultUrl + '/spread');

        this.setState({ hubConnection }, () => {
            this.state.hubConnection
                .start()
                .then(() => console.log('Connection started!'))
                .catch(err => console.log('Error while establishing connection :('));

            this.state.hubConnection.on('new', (receivedMessage) => {
                this.addOrUpdateSpreadPair(this.createSpreadObj(receivedMessage));
            });
        });
    };
    
    createSpreadObj = (obj) => ({
        Spread: obj.spread,
        BuyStock: stockTypes[obj.ask.stock],
        SellStock: stockTypes[obj.bid.stock],
        BuyPair: obj.ask.currencyPair.toLowerCase(),
        SellPair: obj.bid.currencyPair.toLowerCase(),
        SellQty: obj.bid.qty,
        SellRate: obj.bid.rate,
        BuyQty: obj.ask.qty,
        BuyRate: obj.ask.rate
    });
    
    addOrUpdateSpreadPair = (newSpread) => {
        let buyPair = newSpread.BuyPair.toLowerCase();
        let sellPair = newSpread.SellPair.toLowerCase();
        let elIndex = this.state.spreads.findIndex(el => 
            (el.BuyPair === buyPair || el.BuyPair === sellPair));
        if (elIndex === -1) {
            this.setState(prevState => ({
                spreads:[...prevState.spreads, newSpread]
            }));
        } else {
            let spreadsCopy = JSON.parse(JSON.stringify(this.state.spreads));
            spreadsCopy[elIndex] = newSpread;
            this.setState({spreads: spreadsCopy})
        }
        this._rows = this.state.spreads;
    };
    
    rowGetter = (i) => {
        return this._rows[i];
    };
    
    switchModal = () => {
        this.setState({
            modalOpened: !this.state.modalOpened
        })
    };
    
    addNewPair = (obj) => {
        let newSpreadObj = {
            Spread: 0,
            BuyStock: 'HitBtc',
            SellStock: 'Bittrex',
            BuyPair: `${obj.hitBase.toLowerCase()}_${obj.hitQuote.toLowerCase()}`,
            SellPair: `${obj.bitQuote.toLowerCase()}_${obj.bitBase.toLowerCase()}`,
            SellQty: 'N/A',
            SellRate: 'N/A',
            BuyQty: 'N/A',
            BuyRate: 'N/A'
        };
        this.addOrUpdateSpreadPair(newSpreadObj);
        this.setState({
            modalOpened: !this.state.modalOpened
        });
    };
    
    render() {
        return (
            <div>
                <NewPair opened={this.state.modalOpened} addPair={this.addNewPair} close={this.switchModal}/>
                <div className="addButtonContainer">
                    <button type="button" className="btn btn-success" onClick={this.switchModal}>Add Pair</button>
                </div>
                <div className="gridContainer">
                <ReactDataGrid
                    columns={this._columns}
                    rowGetter={this.rowGetter}
                    rowsCount={this._rows.length} />
                </div>
            </div>
        );
    }
}

export default Spreads;
