import React, { Component } from 'react';
import './NewPair.css';
import {defaultUrl, stockTypes} from "../../../constants";
import Modal from 'react-modal';
import PropTypes from 'prop-types';
import {postData} from "../../../utils";
import { toast } from 'react-toastify';

class NewPair extends Component {
    constructor(props){
        super(props);
        // this.state = {
        //     // base: '',
        //     // quote: '',
        //     // stock:'HitBtc'
        // };
        this.state = this.getDefaultState();
    }

    getDefaultState = () => ({
        hitBase: '',
        hitQuote: '',
        bitBase: '',
        bitQuote: '',
        tickSize: 0
    });
    
    checkPair = () => {
        const params = `?hitBase=${this.state.hitBase}&hitQuote=${this.state.hitQuote}&`
            + `bitBase=${this.state.bitBase}&bitQuote=${this.state.bitQuote}`;
        console.log('params', params);
        fetch(defaultUrl + '/api/AlicaControl/checkcurrency' + params).then((response) => {
            response.json().then((data)=>{
                console.log('data', data);
                if (data)
                    this.addPair();
                else 
                    toast.error('Pairs don\'t exist');
            })
        });
    };
    
    addPair = () => {
        postData(defaultUrl+'/api/AlicaControl/addcurrency', this.state).then(()=>{
            toast.success('Pair added successfully');
            this.props.addPair(this.state);
            this.setState(this.getDefaultState());
        }).catch(error => toast.error(error.message))
    };
    
    handleInputChange = (event) => {
      const name = event.target.name;
      const value = event.target.value;
      this.setState({
          [name]: value
      })
    };
    
    renderStockTypes = () => {
        return Object.entries(stockTypes).map(([key, value], i) => {
            return <option key={key} value={key}>{value}</option>
        });
    };
    
    render() {
        return (
            <div className="container">
                <Modal
                    isOpen={this.props.opened}
                    contentLabel="New Pair Modal"
                    ariaHideApp={false}
                    style={
                        {content:{
                            maxWidth:'20%',
                                marginLeft:'auto',
                                marginRight: 'auto',
                                maxHeight: '470px'
                            }}
                    }
                >
                    {/*<label>Stock</label>*/}
                    {/*<select name="stock" onChange={this.handleInputChange} className="form-control">*/}
                        {/*{this.renderStockTypes()}*/}
                    {/*</select>*/}
                    {/*<label>Base</label>*/}
                    {/*<input type="text" className="form-control" name="base" value={this.state.base} onChange={this.handleInputChange}/>*/}
                    {/*<label>Quote</label>*/}
                    {/*<input type="text" className="form-control" name="quote" value={this.state.quote} onChange={this.handleInputChange}/>*/}
                    <label>Hit Base</label>
                    <input type="text" className="form-control" name="hitBase" value={this.state.hitBase} onChange={this.handleInputChange}/>
                    <label>Hit Quote</label>
                    <input type="text" className="form-control" name="hitQuote" value={this.state.hitQuote} onChange={this.handleInputChange}/>
                    <label>Bit Base</label>
                    <input type="text" className="form-control" name="bitBase" value={this.state.bitBase} onChange={this.handleInputChange}/>
                    <label>Bit Quote</label>
                    <input type="text" className="form-control" name="bitQuote" value={this.state.bitQuote} onChange={this.handleInputChange}/>
                    <label>Tick size</label>
                    <input type="text" className="form-control" name="tickSize" value={this.state.tickSize} onChange={this.handleInputChange}/>
                    <br/>
                    <div className="modal-buttons">
                    <button onClick={this.props.close} type="button" className="btn btn-warning">Close</button>
                    <button type="button" onClick={this.checkPair} className="btn btn-success">Save</button>
                    </div>
                </Modal>
            </div>
        );
    }
}

NewPair.propTypes = {
    opened: PropTypes.bool,
    close: PropTypes.func,
    addPair: PropTypes.func
};

export default NewPair;
