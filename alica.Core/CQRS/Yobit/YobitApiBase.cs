﻿using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using alica.Data.Infrastructure;
using Microsoft.Extensions.Options;

namespace alica.Core.CQRS.Yobit
{
    public class YobitApiBase
    {
        protected static readonly ConfigurationOptions _configurationOptions;
        protected static readonly string _apiUrl;
        protected static readonly string _tradeApiUrl;
        protected static readonly HttpClient _client;
        
        static YobitApiBase()
        {
            _client = new HttpClient();
            _configurationOptions = AlicaServiceLocator.GetService<IOptions<ConfigurationOptions>>().Value;
            _apiUrl = _configurationOptions.YobitApiBasicUrl;
            _tradeApiUrl = _configurationOptions.YobitApiTradeUrl;
        }
        
        protected static void SignUrl(HttpRequestMessage request)
        {
            var keyByte = Encoding.UTF8.GetBytes(_configurationOptions.YobitSecret);
            var hmacsha512 = new HMACSHA512(keyByte);
            var messageBytes = Encoding.UTF8.GetBytes(request.RequestUri.ToString());
            var hashmessage = hmacsha512.ComputeHash(messageBytes);
            request.Headers.Add("Key", _configurationOptions.YobitApiKey);
            request.Headers.Add("Sign", ByteToString(hashmessage));
        }
        private static string ByteToString(byte[] buff)
        {
            var sbinary = "";
            for (var i = 0; i < buff.Length; i++)
                sbinary += buff[i].ToString("X2"); /* hex format */
            return sbinary;
        }   
    }
}