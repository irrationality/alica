﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Yobit
{
    [RegisterToDi]
    public class YobitBalanceQuery : YobitApiBase
    {
        private readonly ILogger _logger;
        
        public YobitBalanceQuery()
        {
            _logger = Log.ForContext<YobitBalanceQuery>();
        }

        public async Task<GetBalanceResult> ExecuteAsync(string currency)
        {
            _logger.Information("Getting balances by API key");
            
            var nonce = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            var url = $"{_tradeApiUrl}/getInfo?nonce={nonce}";
            var req = new HttpRequestMessage(HttpMethod.Get, url);
            SignUrl(req);
            
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
            _logger.Information("Response: {json}", str);

            if ((int) json["success"] != 1)
            {
                _logger.Error("Api returned error: {error}", str);
                throw new Exception(str);
            }

            var available = json["result"]["funds"][currency];
            var balance = new GetBalanceResult(
                currency,
                available.Type == JTokenType.Null ? 0 : (decimal)available,
                0.0m
            );
            _logger.Information("Returning balance: {@balance}", balance);
            return balance;
        }
    }
}