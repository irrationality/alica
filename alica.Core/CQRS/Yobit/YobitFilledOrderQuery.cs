﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Core.CQRS.Interfaces;
using alica.Data.Infrastructure;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Yobit
{
    [RegisterToDi]
    public class YobitFilledOrderQuery : YobitApiBase, IFilledOrderQuery
    {
        private readonly ILogger _logger;
        
        public YobitFilledOrderQuery() 
        {
            _logger = Log.ForContext<YobitFilledOrderQuery>();
        }

        public async Task<decimal> ExecuteAsync(string id)
        {
            _logger.Information("Getting balances by API key");
            var nonce = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            var url = $"{_tradeApiUrl}/OrderInfo?order_id={id}&nonce={nonce}";
            var req = new HttpRequestMessage(HttpMethod.Get, url);

            SignUrl(req);
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
            
            if ((int) json["success"] != 1)
            {
                _logger.Error("Api returned error: {error}", str);
                throw new Exception(str);
            }

            var order = json["return"][id];
            var result = (decimal) order["amount"];
            return result;
        }
    }
}