﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Yobit
{
    [RegisterToDi]
    public class YobitWithdrawCommand : YobitApiBase
    {
        private readonly ILogger _logger;

        public YobitWithdrawCommand()
        {
            _logger = Log.ForContext<YobitWithdrawCommand>();
        }

        public async Task ExecuteAsync(string currency, decimal quantity)
        {
            var address = _configurationOptions.Wallets[StockType.Yobit]
                .FirstOrDefault(x => x.Currency == currency).Address;

            _logger.Information("Withdrawing funds for {currency} in quantity {quantity} on address {address}",
                currency, quantity, address);
            var nonce = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            var baseUrl = $"{_tradeApiUrl}/WithdrawCoinsToAddress?nonce={nonce}";
            baseUrl += $"&coinName={currency}&amount={quantity}&address={address}";
            var req = new HttpRequestMessage(HttpMethod.Get, baseUrl);
            SignUrl(req);

            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
            if ((int) json["success"] != 1)
            {
                _logger.Error("Error: {error}", str);
                _logger.Error("Api returned error: {error}", str);
                throw new Exception(str);
            }
        }
    }
}