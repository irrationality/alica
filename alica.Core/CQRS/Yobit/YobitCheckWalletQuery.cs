﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Yobit
{
    [RegisterToDi]
    public class YobitCheckWalletQuery : YobitApiBase
    {
        private readonly ILogger _logger;
        
        public YobitCheckWalletQuery() 
        {
            _logger = Log.ForContext<YobitCheckWalletQuery>();
        }
        
        public async Task<CheckWalletResult> QueryAsync(string currency)
        {
            _logger.Information("Checking wallet health");
            var req = new HttpRequestMessage(HttpMethod.Get, $"{_apiUrl}/info");
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);

            var wallet = json["pairs"][currency];
            var enabled = !(bool) wallet["hidden"];
            
            var res = new CheckWalletResult(currency, enabled, enabled);
            _logger.Information("Response: {@res}", res);
            return res;
        }
    }
}