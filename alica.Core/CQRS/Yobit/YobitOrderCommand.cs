﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Yobit
{
    [RegisterToDi]
    public class YobitOrderCommand : YobitApiBase
    {
        private readonly ILogger _logger;
        
        public YobitOrderCommand()
        {
            _logger = Log.ForContext<YobitOrderCommand>();
        }

        public async Task<string> CreateAsync(string baseCurrency, string quoteCurrency,
            decimal quantity, decimal price, SideType type)
        {
            var pair = $"{baseCurrency}_{quoteCurrency}";
            _logger.Information("Creating {type} order for {symbol} with quatity {quantity} and price {price}",
                type, pair, quantity, price);
            
            var nonce = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            var baseUrl = $"{_tradeApiUrl}/trade?nonce={nonce}";
            baseUrl += $"&pair={pair}&amount={quantity}&rate={price}&type={type.ToString()}";
            var req = new HttpRequestMessage(HttpMethod.Get, baseUrl);
            SignUrl(req);
            
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
            
            if ((decimal) json["success"] != 1)
            {
                _logger.Error("Error: {error}", str);
                _logger.Error("Api returned error: {error}", str);
                throw new Exception(str);
            }
           
            _logger.Information("Result: {json}", (string) json["return"]["order_id"]);
            return (string) json["return"]["order_id"];
        }

        public async Task<bool> CancelAsync(string id)
        {
            _logger.Information("Cancelling order with id {id}", id);
           
            var nonce = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            var baseUrl = $"{_tradeApiUrl}/CancelOrder?order_id={id}&nonce={nonce}";
            var req = new HttpRequestMessage(HttpMethod.Get, baseUrl);
            SignUrl(req);
            
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
           
            if ((int) json["success"] != 1)
            {
                _logger.Error("Api returned error: {error}", str);
                throw new Exception(str);
            }
            _logger.Information("Result: {json}", str);
            return true;
        }
    }
}