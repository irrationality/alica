﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Yobit
{
    [RegisterToDi]
    public class YobitMinBaseLotQuery : YobitApiBase
    {
        private readonly ILogger _logger;
        
        public YobitMinBaseLotQuery() 
        {
            _logger = Log.ForContext<YobitMinBaseLotQuery>();
        }
        
        public async Task<decimal> ExecuteAsync(string baseCurrency, string quoteCurrency)
        {
            _logger.Information("Getting Minimal base lot");
            
            var req = new HttpRequestMessage(HttpMethod.Get, $"{_apiUrl}/info");
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
            
            var wallet = json["pairs"][$"{baseCurrency}_{quoteCurrency}"];
           
            _logger.Information("Minimal trade size is {size}", (decimal)wallet["min_amount"]);
            
            return (decimal) wallet["min_amount"];
        }
    }
}