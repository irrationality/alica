﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.HitBtc
{
    [RegisterToDi]
    public class HitBalanceQuery : HitBtcApiBase
    {
        private readonly ILogger _logger;
        public HitBalanceQuery()
        {
        
            _logger = Log.ForContext<HitBalanceQuery>();
        }

        public async Task<ICollection<GetBalanceResult>> ExecuteAsync()
        {
            _logger.Information("Getting balance");
            var req = new HttpRequestMessage(HttpMethod.Get, $"{_apiUrl}/trading/balance");
            var resp = await client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            
            if (str.Contains("error"))
            {
                _logger.Error("Api returned error {error}", str);
                throw new Exception(str);
            }
            
            var json = JArray.Parse(str);
         
            var balances = json
                .Select(x => new GetBalanceResult(
                    (string) x["currency"],
                    (decimal)x["available"],
                    (decimal)x["reserved"]))
                .ToList();
            
            _logger.Information("Returning balances: {@balances}", balances);
            return balances;
        }
        
      
    }
}


