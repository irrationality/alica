﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Core.Infrastructure;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.HitBtc
{
    [RegisterToDi]
    public class HitOrderCommand : HitBtcApiBase
    {
        
        private readonly ILogger _logger;
        
        private const string TimeInForce = "GTC";
        private const string Type = "limit";
        
        public HitOrderCommand() 
        {
            
            _logger = Log.ForContext<HitOrderCommand>();
        }

        public async Task<string> CreateAsync(string baseCurrency, string quoteCurrency,
            decimal quantity, decimal price, SideType type)
        {
            _logger.Information("Creating order");
            var order = new
            {
                symbol = $"{baseCurrency}{quoteCurrency}",
                quantity,
                price,
                type = Type,
                timeInForce = TimeInForce,
                side = type.ToString().ToLower(),
                strictValidate = false
            };
            
            _logger.Information("Order model: {@order}", order);
        
            var req = new HttpRequestMessage(HttpMethod.Post,$"{_apiUrl}/order") {Content = new JsonContent(order) };
            var resp = await client.SendAsync(req);
            
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
            
            _logger.Information("Result: {json}", str);
           
            if (json["error"] == null) return (string) json["clientOrderId"];
           
            _logger.Error("Api returned error {code}: {error} {description}",
                json["error"]["code"], (string)json["error"]["message"], json["error"]["description"]);
            throw new Exception($"{json["error"]["message"]}; {json["error"]["description"]}");
        }
        
        public async Task<bool> CancelAsync(string id)
        {
            _logger.Information("Cancelling order with id {id}", id);
            var req = new HttpRequestMessage(HttpMethod.Delete, $"{_apiUrl}/order/{id}" );
            var resp = await client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
            _logger.Information("Result: {json}", str);
           
            if (json["error"] == null) return json["status"].ToString().ToLower() == "canceled";
            
            _logger.Error("Api returned error {code}: {error} {description}",
                json["error"]["code"], (string)json["error"]["message"], json["error"]["description"]);
            throw new Exception($"{json["error"]["message"]}; {json["error"]["description"]}");
        }
    }
}