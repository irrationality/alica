﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.HitBtc
{
    [RegisterToDi]
    public class HitCheckWalletQuery : HitBtcApiBase
    {
        private readonly ILogger _logger;

        public HitCheckWalletQuery() 
        {
            _logger = Log.ForContext<HitCheckWalletQuery>();
        }

        public async Task<CheckWalletResult> ExecuteAsync(string currency)
        {
            _logger.Information("Checking wallet health");
            var req = new HttpRequestMessage(HttpMethod.Get, $"{_apiUrl}/public/currency/{currency}");
            var resp = await client.SendAsync(req);
            var json = JObject.Parse(await resp.Content.ReadAsStringAsync());

            if (json["error"] != null)
            {
                _logger.Error("Api returned error {code}: {error} {description}",
                    json["error"]["code"], (string) json["error"]["message"], json["error"]["description"]);
                throw new Exception($"{json["error"]["message"]}; {json["error"]["description"]}");
            }

            var deposit = (bool) json["payinEnabled"];
            var withdraw = (bool) json["payoutEnabled"];

            var res = new CheckWalletResult(currency, deposit, withdraw);
            _logger.Information("Check wallet result {@wallet}", res);
            return res;
        }
    }
}