﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.HitBtc
{
    [RegisterToDi]
    public class HitMinBaseLotQuery : HitBtcApiBase
    {
        private readonly ILogger _logger;
     
        public HitMinBaseLotQuery()
        {
            _logger = Log.ForContext<HitMinBaseLotQuery>();
        }

        public async Task<decimal> ExecuteAsync(string symbol)
        {
            _logger.Information("Getting minimal base lot");
            var req = new HttpRequestMessage(HttpMethod.Get, $"{_apiUrl}/public/symbol/{symbol.ToUpper()}");
            var resp = await client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
            _logger.Information("Result: {json}", str);

            if (json["error"] == null)
            {
                _logger.Information("Base lot {baselot}", (decimal) json["quantityIncrement"]);
                return (decimal) json["quantityIncrement"];
            }
            
            _logger.Error("Api returned error {code}: {error} {description}",
                json["error"]["code"], (string)json["error"]["message"], json["error"]["description"]);
            throw new Exception($"{json["error"]["message"]}; {json["error"]["description"]}");
        }
    }
}