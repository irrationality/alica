﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using alica.Core.Infrastructure;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.HitBtc
{
    [RegisterToDi]
    public class HitWithdrawCommand : HitBtcApiBase
    {
        private readonly ILogger _logger;
        
        public HitWithdrawCommand() 
        {
            _logger = Log.ForContext<HitWithdrawCommand>();
        }

        public async Task<bool> ExecuteAsync(string currency, decimal amount)
        {
            var address = options.Wallets[StockType.Bittrex]
                .FirstOrDefault(x => x.Currency == currency).Address;
            _logger.Information("Withdrawing funds");
            var withdraw = new
            {
                currency,
                amount,
                address
            };
            _logger.Information("Withdraw: {@withdraw}", withdraw);
            
            var req = new HttpRequestMessage(HttpMethod.Post, $"{_apiUrl}/order") { Content = new JsonContent(withdraw)};
            var resp = await client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
           
            _logger.Information("Result: {json}", str);
            
            if (json["error"] == null) return json["error"] == null;
            
            _logger.Error("Api returned error {code}: {error} {description}",
                json["error"]["code"], (string)json["error"]["message"], json["error"]["description"]);
            throw new Exception($"{json["error"]["message"]}; {json["error"]["description"]}");
        }
    }
}