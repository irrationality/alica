﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using alica.Data.Infrastructure;
using Microsoft.Extensions.Options;

namespace alica.Core.CQRS.HitBtc
{
    public class HitBtcApiBase
    {
        protected static readonly HttpClient client;
        protected static readonly ConfigurationOptions options;
        protected static readonly string _apiUrl;
        
        static  HitBtcApiBase()
        {
            options = AlicaServiceLocator.GetService<IOptions<ConfigurationOptions>>().Value;
            _apiUrl = options.HitBtcApiBasicUrl;
            client = new HttpClient();
            var authByteArray = Encoding.ASCII.GetBytes(options.HitBtcApiKey);
            client.DefaultRequestHeaders.Authorization =  new AuthenticationHeaderValue("Basic", Convert.ToBase64String(authByteArray));
        }
    }
}