﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Core.CQRS.Interfaces;
using alica.Data.Infrastructure;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.HitBtc
{
    [RegisterToDi]
    public class HitFilledOrderQuery : HitBtcApiBase, IFilledOrderQuery
    {
        private readonly ILogger _logger;
        
        public HitFilledOrderQuery()
        {
            _logger = Log.ForContext<HitFilledOrderQuery>();
        }

        public async Task<decimal> ExecuteAsync(string id)
        {
            _logger.Information("Getting order with id {id}", id);
            
            var req = new HttpRequestMessage(HttpMethod.Get, $"{_apiUrl}/history/order?clientOrderId={id}");
            var resp = await client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync(); 
            
            if (str.Contains("error"))
            {
                _logger.Error("Api returned error {error}", str);
                throw new Exception(str);
            }
            
            var json = JArray.Parse(str);
            var orderJson = json.FirstOrDefault();
            var result = (decimal) orderJson["quantity"] - (decimal) orderJson["cumQuantity"];
            return result;
        }
    }
}