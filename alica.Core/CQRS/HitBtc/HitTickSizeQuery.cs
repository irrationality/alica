﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.HitBtc
{
    [RegisterToDi]
    public class HitTickSizeQuery : HitBtcApiBase
    {
        private readonly ILogger _logger;
        
        public HitTickSizeQuery()
        {
            _logger = Log.ForContext<HitTickSizeQuery>();
        }

        public async Task<Decimal> ExecuteAsync(string baseCurrency, string quoteCurrency)
        {
            _logger.Information("Getting tick size by symbol {symbol}", $"{baseCurrency}{quoteCurrency}");
            var req = new HttpRequestMessage(HttpMethod.Get, $"{_apiUrl}/public/symbol/{baseCurrency}{quoteCurrency}");
            var resp = await client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
            
            _logger.Information("Result: {json}", str);
            if (json["error"] != null)
            {
                _logger.Error("Api returned error {code}: {error} {description}",
                    json["error"]["code"], (string)json["error"]["message"], json["error"]["description"]);
                throw new Exception($"{json["error"]["message"]}; {json["error"]["description"]}");
            }
            _logger.Information("Returning tick size {tickSize}", (decimal)json["tickSize"]);
            return (decimal)json["tickSize"];
        }
    }
}