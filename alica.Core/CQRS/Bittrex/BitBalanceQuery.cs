﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Bittrex
{
    [RegisterToDi]
    public class BitBalanceQuery : BittrexApiBase
    {
        private readonly ILogger _logger;
        
        public BitBalanceQuery()
        {
            _logger = Log.ForContext<BitBalanceQuery>();
        }

        public async Task<GetBalanceResult> ExecuteAsync(string currency)
        {
            _logger.Information("Getting balances by API key");
            
            var nonce = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            var url = $"{_apiUrl}/account/getbalance?{ApiKeyUrl(nonce)}&currency={currency}";
            var req = new HttpRequestMessage(HttpMethod.Get, url);
            SignUrl(req);
            
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
            _logger.Information("Response: {json}", str);

            if (!(bool) json["success"])
            {
                _logger.Error("Api returned error: {error}", (string) json["message"]);
                throw new Exception((string) json["message"]);
            }

            var available = json["result"]["Available"];
            var balance = new GetBalanceResult(
                (string) json["result"]["Currency"],
                available.Type == JTokenType.Null ? 0 : (decimal)available,
                0.0m
            );
            _logger.Information("Returning balance: {@balance}", balance);
            return balance;
        }
    }
}