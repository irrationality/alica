﻿using System;
using System.Net.Http;
using alica.Data.Infrastructure;


namespace alica.Core.CQRS.Bittrex
{
    [RegisterToDi]
    public class BitMarketQuery : IDisposable
    {
        private readonly HttpClient _httpClient;
        private readonly string _apiUrl;
        
        public BitMarketQuery(ConfigurationOptions configurationOptions)
        {
            _httpClient = new HttpClient();   
            _apiUrl = configurationOptions.BittrexApiBasicUrl;
        }
        
        
        public void Dispose()
        {
            _httpClient?.Dispose();
        }
    }
}