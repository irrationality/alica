﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Bittrex
{
    [RegisterToDi]
    public class BitWithdrawCommand : BittrexApiBase
    {
        
        private readonly ILogger _logger;
        
        public BitWithdrawCommand()
        {
            _logger = Log.ForContext<BitWithdrawCommand>();
        }

        public async Task ExecuteAsync(string currency, decimal quantity)
        {
            var address = _configurationOptions.Wallets[StockType.HitBtc]
                .FirstOrDefault(x => x.Currency == currency).Address;
            
            _logger.Information("Withdrawing funds for {currency} in quantity {quantity} on address {address}",
                currency, quantity, address);
            var nonce = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(); 
            var baseUrl = $"{_apiUrl}/account/withdraw?{ApiKeyUrl(nonce)}";
            baseUrl += $"&currency={currency}&quantity={quantity}&address={address}";
            var req = new HttpRequestMessage(HttpMethod.Get, baseUrl);
            SignUrl(req);
           
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);           
            if (!(bool) json["success"])
            {
                _logger.Error("Error: {error}", str);
                _logger.Error("Api returned error: {error}", (string)json["message"]);
                throw new Exception((string)json["message"]);
            }
        }
    }
}