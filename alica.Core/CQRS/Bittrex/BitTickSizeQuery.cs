﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Bittrex
{
    [RegisterToDi]
    public class BitTickSizeQuery : BittrexApiBase
    {
        private readonly ILogger _logger;
        
        public BitTickSizeQuery()
        {
            _logger = Log.ForContext<BitTickSizeQuery>();
        }
        
        public async Task<ICollection<TickSize>> AllSizesQueryAsync()
        {
            _logger.Information("Getting all tick sizes");
            var req = new  HttpRequestMessage(HttpMethod.Get, $"{_apiUrl}/public/getmarkets");
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
            
            if (!(bool) json["success"])
            {
                _logger.Error("Error: {error}", str);
                _logger.Error("Api returned error: {error}", (string)json["message"]);
                throw new Exception((string)json["message"]);
            }
            
            var tickSizes = json["result"]
                .Select(x => new TickSize(
                    GetBaseCurrency(x["MarketName"]),
                    GetQuoteCurrency(x["MarketName"]),
                    0.000001m,
                    StockType.Bittrex))
                .ToList();
            _logger.Information("Tick sizes {@tickSizes}", tickSizes);
            return tickSizes;
        }

        public async Task<Decimal> ExecuteAsync(string baseCurrency, string quoteCurrency)
        {
            
            _logger.Information("Getting tick size");
            var req = new  HttpRequestMessage(HttpMethod.Get, $"{_apiUrl}/public/getmarketsummary?market={quoteCurrency}-{baseCurrency}");
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
            
            if (!(bool) json["success"])
            {
                _logger.Error("Error: {error}", str);
                _logger.Error("Api returned error: {error}", (string)json["message"]);
                throw new Exception((string)json["message"]);
            }

            var scaledOne = GetScaledOne((decimal) json["result"].FirstOrDefault()["Last"]);
            _logger.Information("Tick size {tickSize}", scaledOne);
            return scaledOne;
        }


        //Getting from the string of type "BTC-ETC" ETC value
        private static string GetBaseCurrency(JToken field)
        {
            var value = (string) field;
            return value.Split('-')[1];
        }

        //Getting from the string of type "BTC-ETC" BTC value
        private static string GetQuoteCurrency(JToken field)
        {
            var value = (string) field;
            return value.Split('-')[0];
        }
        
        public decimal GetScaledOne(decimal value)
        {
            int[] bits = decimal.GetBits(value);
            // Generate a value +1, scaled using the same scaling factor as the input value
            bits[0] = 1;
            bits[1] = 0;
            bits[2] = 0;
            bits[3] = bits[3] & 0x00FF0000;
            return new decimal(bits);
        }
    }
}