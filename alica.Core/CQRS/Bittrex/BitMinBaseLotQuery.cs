﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Bittrex
{
    [RegisterToDi]
    public class BitMinBaseLotQuery : BittrexApiBase
    {
        private readonly ILogger _logger;
        
        public BitMinBaseLotQuery() 
        {
            _logger = Log.ForContext<BitMinBaseLotQuery>();
        }
        
        public async Task<decimal> ExecuteAsync(string baseCurrency, string quoteCurrency)
        {
            _logger.Information("Getting Minimal base lot");
            
            var req = new HttpRequestMessage(HttpMethod.Get, $"{_apiUrl}/public/getmarkets");
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
           
            if (!(bool) json["success"])
            {
                _logger.Error("Error: {error}", str);
                _logger.Error("Api returned error: {error}", (string)json["message"]);
                throw new Exception((string)json["message"]);
            }
            var market = json["result"]
                .FirstOrDefault(x => string.Equals((string) x["MarketCurrency"], baseCurrency, StringComparison.CurrentCultureIgnoreCase) ||
                                     string.Equals((string) x["BaseCurrency"], quoteCurrency, StringComparison.CurrentCultureIgnoreCase));
           
            _logger.Information("Minimal trade size is {size}", (decimal)market["MinTradeSize"]);
            
            return (decimal) market["MinTradeSize"];
        }
    }
}