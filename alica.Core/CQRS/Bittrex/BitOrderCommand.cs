﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Bittrex
{
    [RegisterToDi]
    public class BitOrderCommand : BittrexApiBase
    {
        
        private readonly ILogger _logger;
        
        public BitOrderCommand()
        {
            _logger = Log.ForContext<BitOrderCommand>();
        }

        public async Task<string> CreateAsync(string baseCurrency, string quoteCurrency,
            decimal quantity, decimal price, SideType type)
        {
            var symbol = $"{quoteCurrency}-{baseCurrency}";
            _logger.Information("Creating {type} order for {symbol} with quatity {quantity} and price {price}",
                type, symbol, quantity, price);
            
            var nonce = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            var baseUrl = $"{_apiUrl}/market/{type.ToString().ToLower()}limit?" +
                          $"{ApiKeyUrl(nonce)}";
            baseUrl += $"&market={symbol}&quantity={quantity}&rate={price}";
            var req = new HttpRequestMessage(HttpMethod.Get, baseUrl);
            SignUrl(req);
            
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
            
            if (!(bool) json["success"])
            {
                _logger.Error("Error: {error}", str);
                _logger.Error("Api returned error: {error}", (string) json["message"]);
                throw new Exception((string) json["message"]);
            }
           
            _logger.Information("Result: {json}", (string) json["result"]["OrderId"]);
            return (string) json["result"]["uuid"];
        }

        public async Task<bool> CancelAsync(string id)
        {
            _logger.Information("Cancelling order with id {id}", id);
           
            var nonce = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            var baseUrl = $"{_apiUrl}/market/cancel?{ApiKeyUrl(nonce)}&uuid={id}";
            var req = new HttpRequestMessage(HttpMethod.Get, baseUrl);
            SignUrl(req);
            
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
           
            if (!(bool) json["success"])
            {
                _logger.Error("Api returned error: {error}", (string) json["message"]);
                throw new Exception((string) json["message"]);
            }
            _logger.Information("Result: {json}", str);
            return true;
        }
    }
}