﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Core.CQRS.Interfaces;
using alica.Data.Infrastructure;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Bittrex
{
    [RegisterToDi]
    public class BitFilledOrderQuery : BittrexApiBase, IFilledOrderQuery
    {
        private readonly ILogger _logger;
        
        public BitFilledOrderQuery() 
        {
            _logger = Log.ForContext<BitFilledOrderQuery>();
        }

        public async Task<decimal> ExecuteAsync(string id)
        {
            _logger.Information("Getting balances by API key");
            var nonce = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            var url = $"{_apiUrl}/account/getorder?uuid={id}&{ApiKeyUrl(nonce)}";
            var req = new HttpRequestMessage(HttpMethod.Get, url);

            SignUrl(req);
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
            if (!(bool) json["success"])
            {
                _logger.Error("Error: {error}", str);
                _logger.Error("Api returned error: {error}", (string) json["message"]);
                throw new Exception((string) json["message"]);
            }

            var order = json["result"];
            var result = (decimal) order["QuantityRemaining"];
            return result;
        }
    }
}