﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Bittrex
{
    [RegisterToDi]
    public class BitCheckWalletQuery : BittrexApiBase
    {
        private readonly ILogger _logger;
        
        public BitCheckWalletQuery() 
        {
            _logger = Log.ForContext<BitCheckWalletQuery>();
        }
        
        public async Task<CheckWalletResult> QueryAsync(string currency)
        {
            _logger.Information("Checking wallet health");
            var req = new HttpRequestMessage(HttpMethod.Get, "https://bittrex.com/api/v2.0/pub/currencies/GetWalletHealth");
            var resp = await _client.SendAsync(req);
            var str = await resp.Content.ReadAsStringAsync();
            var json = JObject.Parse(str);
           
            if (!(bool) json["success"])
            {
                _logger.Error("Error: {error}", str);
                _logger.Error("Api returned error: {error}", (string)json["message"]);
                throw new Exception((string)json["message"]);
            }
            
            var wallet = json["result"].Select(j => j["Health"])
                .FirstOrDefault(j => string.Equals((string) j["Currency"], currency,StringComparison.InvariantCultureIgnoreCase));
            var enabled = (bool) wallet["IsActive"];
            
            var res = new CheckWalletResult(currency, enabled, enabled);
            _logger.Information("Response: {@res}", res);
            return res;
        }
        
      
    }
}