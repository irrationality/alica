﻿using System.Threading.Tasks;

namespace alica.Core.CQRS.Interfaces
{
    public interface IFilledOrderQuery
    {
        Task<decimal> ExecuteAsync(string id);
    }
}