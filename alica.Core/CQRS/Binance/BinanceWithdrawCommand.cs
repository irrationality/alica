﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Binance
{
    [RegisterToDi]
    public class BinanceWithdrawCommand : BinanceApiBase
    {
        private readonly ILogger _logger;
        
        public BinanceWithdrawCommand()
        {
            _logger = Log.ForContext<BinanceWithdrawCommand>();
        }

        public async Task ExecuteAsync(string currency, decimal quantity)
        {
            var address = _configurationOptions.Wallets[StockType.Binance]
                .FirstOrDefault(x => x.Currency == currency).Address;
            
            _logger.Information("Withdrawing funds for {currency} in quantity {quantity} on address {address}",
                currency, quantity, address);
            await _client.Withdraw(currency, quantity, address);
        }
    }
}