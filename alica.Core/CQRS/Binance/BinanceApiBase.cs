﻿using alica.Data.Infrastructure;
using Binance.API.Csharp.Client;
using Microsoft.Extensions.Options;

namespace alica.Core.CQRS.Binance
{
    public class BinanceApiBase
    {
        protected static readonly ConfigurationOptions _configurationOptions;
        protected static readonly BinanceClient _client;
        
        static BinanceApiBase()
        {
            _configurationOptions = AlicaServiceLocator.GetService<IOptions<ConfigurationOptions>>().Value;
            var apiClient = new ApiClient(_configurationOptions.BinanceApiKey, _configurationOptions.BinanceSecret);
            _client = new BinanceClient(apiClient);
        }
  
    }
}