﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Binance
{
    [RegisterToDi]
    public class BinanceFilledOrderQuery : BinanceApiBase
    {
        private readonly ILogger _logger;
        
        public BinanceFilledOrderQuery() 
        {
            _logger = Log.ForContext<BinanceFilledOrderQuery>();
        }

        public async Task<bool> ExecuteAsync(string baseCurrency, string quoteCurrency, long id)
        {
            _logger.Information("Getting balances by API key");
            var order = await _client.GetOrder($"{baseCurrency.ToUpper()}{quoteCurrency.ToUpper()}", id);
            _logger.Information("Result: {@order}", order);
            var result = order.Status.ToUpper() == "FILLED";
            return result;
        }
    }
}