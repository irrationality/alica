﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Binance
{
    [RegisterToDi]
    public class BinanceCheckWalletQuery : BinanceApiBase
    {
        private readonly ILogger _logger;
        
        public BinanceCheckWalletQuery() 
        {
            _logger = Log.ForContext<BinanceCheckWalletQuery>();
        }
        
        public async Task<CheckWalletResult> QueryAsync(string currency)
        {
            throw new NotImplementedException();
        }
    }
}