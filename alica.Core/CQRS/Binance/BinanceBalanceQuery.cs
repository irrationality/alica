﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS.Binance
{
    [RegisterToDi]
    public class BinanceBalanceQuery : BinanceApiBase
    {
        private readonly ILogger _logger;
        
        public BinanceBalanceQuery()
        {
            _logger = Log.ForContext<BinanceBalanceQuery>();
        }

        public async Task<GetBalanceResult> ExecuteAsync(string currency)
        {
            _logger.Information("Getting balances by API key");
            var result = await _client.GetAccountInfo();
            var balanceValue = result.Balances.FirstOrDefault(x => x.Asset == currency);
            var balance = new GetBalanceResult(
                currency,
                balanceValue?.Free ?? 0,
                0.0m
            );
            _logger.Information("Returning balance: {@balance}", balance);
            return balance;
        }
    }
}