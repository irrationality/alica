﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Binance.API.Csharp.Client.Models.Enums;
using Newtonsoft.Json.Linq;
using Serilog;
using OrderType = Binance.API.Csharp.Client.Models.Enums.OrderType;

namespace alica.Core.CQRS.Binance
{
    [RegisterToDi]
    public class BinanceOrderCommand : BinanceApiBase
    {
                
        private readonly ILogger _logger;
        
        public BinanceOrderCommand()
        {
            _logger = Log.ForContext<BinanceOrderCommand>();
        }

        public async Task<long> CreateAsync(string baseCurrency, string quoteCurrency,
            decimal quantity, decimal price, SideType type)
        {
            _logger.Information("Creating {type} order for {symbol} with quatity {quantity} and price {price}",
                type, $"{quoteCurrency}{baseCurrency}", quantity, price);
            var side = type == SideType.Buy ? OrderSide.BUY : OrderSide.SELL;
            var result = await _client.PostNewOrder($"{baseCurrency}{quoteCurrency}", quantity, price, side);
            _logger.Information("Order created with id {id}", result.OrderId);
            return result.OrderId;
        }

        public async Task<bool> CancelAsync(long orderId, string baseCurr, string quoteCurr)
        {
            _logger.Information("Cancelling order with id {id}", orderId);
            var result = await _client.CancelOrder($"{baseCurr}{quoteCurr}", orderId);
            _logger.Information("Order cancelled");
            return result.OrderId != 0;
        }
    }
}