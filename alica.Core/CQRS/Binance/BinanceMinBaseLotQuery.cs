﻿using System;
using System.Linq;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using Serilog;

namespace alica.Core.CQRS.Binance
{
    [RegisterToDi]
    public class BinanceMinBaseLotQuery : BinanceApiBase
    {
        private readonly ILogger _logger;
        
        public BinanceMinBaseLotQuery() 
        {
            _logger = Log.ForContext<BinanceMinBaseLotQuery>();
        }
        
        public async Task<decimal> ExecuteAsync(string baseCurrency, string quoteCurrency)
        {
            return _client._tradingRules.Symbols
                .FirstOrDefault(x => x.SymbolName == $"{baseCurrency}{quoteCurrency}")
                .Filters.FirstOrDefault(x => x.FilterType.ToUpper() == "LOT_SIZE")
                .MinQty;
        }
    }
}