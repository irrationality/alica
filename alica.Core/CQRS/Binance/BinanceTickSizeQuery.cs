﻿using System.Linq;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using Serilog;

namespace alica.Core.CQRS.Binance
{
    [RegisterToDi]
    public class BinanceTickSizeQuery:BinanceApiBase
    {
        private readonly ILogger _logger;
        
        public BinanceTickSizeQuery() 
        {
            _logger = Log.ForContext<BinanceTickSizeQuery>();
        }
        
        public async Task<decimal> ExecuteAsync(string baseCurrency, string quoteCurrency)
        {
            return _client._tradingRules.Symbols
                .FirstOrDefault(x => x.SymbolName == $"{baseCurrency}{quoteCurrency}")
                .Filters.FirstOrDefault(x => x.FilterType.ToUpper() == "PRICE_FILTER")
                .TickSize;
        }
    }
}