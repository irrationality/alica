﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Serilog;

namespace alica.Core.CQRS
{
    [RegisterToDi]
    public class CheckCurrencyQuery: IDisposable
    {
        private readonly HttpClient _client;
        private readonly string _bitApi;
        private readonly string _hitApi;
        private readonly ILogger _logger;
        
        public CheckCurrencyQuery()
        {
            var configurationOptions = AlicaServiceLocator.GetService<IOptions<ConfigurationOptions>>().Value;
            _bitApi = configurationOptions.BittrexApiBasicUrl;
            _hitApi = configurationOptions.HitBtcApiBasicUrl;
            _client = new HttpClient();
            _logger = Log.ForContext<CheckCurrencyQuery>();
        }

        public async Task<bool> QueryAsync(string hitBase, string hitQuote, string bitBase, string bitQuote)
        {
            _logger.Information("Checking currencies");
            var hitBtcReq = _client.GetAsync($"{_hitApi}/public/symbol");
            var bittrexReq = _client.GetAsync($"{_bitApi}/public/getmarkets");

            var hitBtcResp = await hitBtcReq;
            var bittrexResp = await bittrexReq;
            
            var hitStr = await hitBtcResp.Content.ReadAsStringAsync();
            if (hitStr.Contains("error"))
            {
                _logger.Error("Api returned error {error}", hitStr);
                throw new Exception(hitStr);
            }
            var hitBtcCurrenciesJson = JArray.Parse(hitStr);

            var bittrexCurrenciesJson = JObject.Parse(await bittrexResp.Content.ReadAsStringAsync());

            if (!(bool) bittrexCurrenciesJson["success"])
            {
                _logger.Error("Api returned error: {error}", (string) bittrexCurrenciesJson["message"]);
                throw new Exception((string) bittrexCurrenciesJson["message"]);
            }

            var hitHasCurrencyPair = hitBtcCurrenciesJson
                .FirstOrDefault(p => 
                    string.Equals((string)p["id"],$"{hitBase}{hitQuote}".ToUpper(), StringComparison.InvariantCultureIgnoreCase));
            
            
            var bitHasCurrencyPair = bittrexCurrenciesJson["result"]
                .FirstOrDefault(p => 
                    string.Equals((string)p["BaseCurrency"], bitQuote, StringComparison.InvariantCultureIgnoreCase) &&
                    string.Equals((string)p["MarketCurrency"], bitBase, StringComparison.InvariantCultureIgnoreCase));
            
            return hitHasCurrencyPair != null && bitHasCurrencyPair != null;
        }

        public void Dispose()
        {
            _client?.Dispose();
        }
    }
}