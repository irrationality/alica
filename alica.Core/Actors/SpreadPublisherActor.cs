﻿using System;
using System.Threading;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Akka.Actor;
using Microsoft.AspNetCore.SignalR;

namespace alica.Core
{
    public class SpreadPublisherActor : ReceiveActor
    {
        public SpreadPublisherActor()
        {
            Receive<Trade>(msg => PublishSpread(msg));
        }

        public void PublishSpread(Trade trade)
        {
            var hubContext = AlicaServiceLocator.GetService<IHubContext<SpreadPublisher>>();
            Console.WriteLine($"Sell {trade.Bid.Rate} Buy {trade.Ask.Rate} spread {trade.Spread}");
            hubContext.Clients.All.InvokeAsync("new", trade);
        }
    }
}