﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Akka.Actor;
using Newtonsoft.Json.Linq;


namespace alica.Core
{
    public class StreamActor : ReceiveActor
    {
        private readonly string _baseHit;
        private readonly string _quoteHit;
        private readonly string _baseBit;
        private readonly string _quoteBit;
        private readonly IActorRef _processActor;
        
        private  string bitPair => $"{_baseBit}_{_quoteBit}";
        private  string hitPair => $"{_baseHit}_{_quoteHit}";
        
        private ClientWebSocket _clientWebSocket;
        private HttpClient _clientHttp;
        private CancellationTokenSource _cts;
        

        private string HitBtcOrderBookSubscribeMsg =>
            $"{{\"method\": \"subscribeOrderbook\",\"params\": {{\"symbol\": \"{_baseHit}{_quoteHit}\"}},\"id\": 123}}";

        private string bittrexGetOrderBookUrl =>
            $"https://bittrex.com/api/v1.1/public/getorderbook?market={_quoteBit}-{_baseBit}&type=both";


        private readonly SortedDictionary<decimal, Order> _hitAsks = new SortedDictionary<decimal, Order>();

        private readonly SortedDictionary<decimal, Order> _hitBids =
            new SortedDictionary<decimal, Order>(new DescendingComparer<decimal>());

        private readonly object _lockAsk = new object();
        private readonly object _lockBid = new object();

        protected override void PreStart()
        {
            _clientHttp = new HttpClient();
            StartStreamingHitBtc();
        }

        protected override void PostStop()
        {
            _cts?.Cancel();
        }

        public StreamActor(string baseHit, string quoteHit, string baseBit, string quoteBit,IActorRef processActor)
        {
            _baseHit = baseHit;
            _quoteHit = quoteHit;
            _baseBit = baseBit;
            _quoteBit = quoteBit;
            _processActor = processActor;
            Become(Streaming);
        }

        private void Streaming()
        {
            Receive<CallBittrex>(msg => _clientHttp
                .GetAsync(bittrexGetOrderBookUrl)
                .ContinueWith(async t => { await CallBittrex(t); }));
        }

        private void StartStreamingHitBtc()
        {
            _cts = new CancellationTokenSource();
            var ct = _cts.Token;

            Task.Run(async () =>
            {
                await ConnectWebSocket(ct);
                var encoded = Encoding.UTF8.GetBytes(HitBtcOrderBookSubscribeMsg);
                var buff = new ArraySegment<byte>(encoded, 0, encoded.Length);
                await _clientWebSocket.SendAsync(buff, WebSocketMessageType.Text, true, ct);
                var buffer = new ArraySegment<byte>(new byte[8192]);
                using (var ms = new MemoryStream())
                {
                    while (!ct.IsCancellationRequested)
                    {
                        if (_clientWebSocket.State != WebSocketState.Open)
                        {
                            await ConnectWebSocket(ct);
                        }
                        ms.SetLength(0);
                        WebSocketReceiveResult result;

                        do
                        {
                            result = await _clientWebSocket.ReceiveAsync(buffer, CancellationToken.None);
                            ms.Write(buffer.Array, buffer.Offset, result.Count);
                        } while (!result.EndOfMessage);

                        if (result.MessageType != WebSocketMessageType.Text) continue;

                        var bytes = ms.ToArray();
                        var str = Encoding.UTF8.GetString(bytes);
                        var hitbtc = JObject.Parse(str);

                        try
                        {
                            var hitBtcAsk = hitbtc["params"]["ask"]
                                .Select(d => new Order(OrderType.Ask, StockType.HitBtc,
                                    (decimal) d["size"], (decimal) d["price"], hitPair)).ToArray();

                            lock (_lockAsk)
                            {
                                foreach (var order in hitBtcAsk)
                                {
                                    if (order.Qty == 0)
                                    {
                                        _hitAsks.Remove(order.Rate);
                                        continue;
                                    }
                                    _hitAsks[order.Rate] = order;
                                }
                            }

                            var hitBtcBid = hitbtc["params"]["bid"]
                                .Select(d => new Order(OrderType.Bid, StockType.HitBtc,
                                    (decimal) d["size"], (decimal) d["price"], hitPair)).ToArray();

                            lock (_lockBid)
                            {
                                foreach (var order in hitBtcBid)
                                {
                                    if (order.Qty == 0)
                                    {
                                        _hitBids.Remove(order.Rate);
                                        continue;
                                    }
                                    _hitBids[order.Rate] = order;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                    await _clientWebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "cancellation requested", ct);
                }
            }, ct);
            Become(Streaming);
        }
        
        private async Task CallBittrex(Task<HttpResponseMessage> t)
        {
            if (!t.Result.IsSuccessStatusCode) return;
            var str = await t.Result.Content.ReadAsStringAsync();
            var bittrex = JObject.Parse(str);
            var bittrexAsk = bittrex["result"]["sell"].Select(d =>
                    new Order(OrderType.Ask, StockType.Bittrex,
                        (decimal) d["Quantity"], (decimal) d["Rate"], bitPair))
                .Take(10).ToArray();
            var bittrexBid = bittrex["result"]["buy"].Select(d =>
                    new Order(OrderType.Bid, StockType.Bittrex,
                        (decimal) d["Quantity"], (decimal) d["Rate"], bitPair))
                .Take(10).ToArray();
            
            if(bittrexAsk.Length == 0 || bittrexBid.Length == 0) return;

            Order[] hitBtcAsk;
            lock (_lockAsk)
            {
                hitBtcAsk = _hitAsks.Values.Take(10).ToArray();
            }

            Order[] hitBtcBid;
            lock (_lockBid)
            {
                hitBtcBid = _hitBids.Values.Take(10).ToArray();
            }
            var message = new OrderBooks(hitBtcAsk, hitBtcBid, bittrexAsk, bittrexBid);
            _processActor.Tell(message);
        }

        private async Task ConnectWebSocket(CancellationToken ct)
        {
            _clientWebSocket?.Dispose();
            _clientWebSocket = new ClientWebSocket();
            await _clientWebSocket.ConnectAsync(new Uri("wss://api.hitbtc.com/api/2/ws"), ct);
        }
    }
}