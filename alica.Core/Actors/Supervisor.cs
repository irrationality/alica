﻿using System.Collections.Generic;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Akka.Actor;
using Akka.Util.Internal;
using Microsoft.Extensions.Options;


namespace alica.Core
{
    public class Supervisor : ReceiveActor
    {
        private readonly Dictionary<string, IActorRef> _currencyPairStreams = new Dictionary<string, IActorRef>();

        public Supervisor()
        {
            
            Receive<AddCurrency>(msg =>
            {
                if (_currencyPairStreams.ContainsKey(msg.Pair)) return;

                var actor = Context.ActorOf(Props.Create(() =>
                    new CurrencyActor(
                       msg.HitBase,
                       msg.HitQuote,
                       msg.BitBase, 
                       msg.BitQuote,
                       msg.TickSize)));
                   
                _currencyPairStreams.Add(msg.Pair, actor);
            });

            
            Receive<RemoveCurrency>(msg =>
            {
                if (!_currencyPairStreams.TryGetValue(msg.Pair, out var actor)) 
                    return;
                _currencyPairStreams.Remove(msg.Pair);
                Context.Stop(actor);
            });

            Receive<CallBittrex>(msg => _currencyPairStreams.ForEach(s =>s.Value.Tell(msg)));
        }
        
    }
}