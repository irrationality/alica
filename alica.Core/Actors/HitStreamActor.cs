﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.WebSockets;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Akka;
using Akka.Streams;
using Akka.Streams.Actors;
using Newtonsoft.Json.Linq;
using Reactive.Streams;

namespace alica.Core
{
    public class OrderBookStream : ISubscriber<Trade>
    {
        private readonly string _baseHit;
        private readonly string _quoteHit;
        private readonly string _baseBit;
        private readonly string _quoteBit;

        public OrderBookStream(
            IMaterializer materializer,
            string baseHit,
            string quoteHit,
            string baseBit,
            string quoteBit)
        {
            _baseHit = baseHit;
            _quoteHit = quoteHit;
            _baseBit = baseBit;
            _quoteBit = quoteBit;
            // var src1 = Source.FromEnumerator(GetHitEnumerator);
            //var src2 = Source.FromEnumerator(GetBitEnumerator);
            // var src1 = Source.FromPublisher(new HitStreamPublisher(_baseHit,_quoteHit));

            //   var src2 = Source.FromPublisher(new BittrexStreamPublisher(_baseBit, _quoteBit));
            //   Source.FromPublisher(new HitStreamPublisher(_baseHit, _quoteHit))
            //       .ZipWith(src2 ,(hit, bit) => new OrderBook(hit.asks, hit.bids, bit.asks, bit.bids))
//                .Select(CountSpread)
//                .RunWith(Sink.FromSubscriber(this),mat); 

//            src1.ZipWith(src2,
//                    (hit, bit) => new OrderBook(hit.asks, hit.bids, bit.asks, bit.bids))
//               // .Throttle(1, TimeSpan.FromSeconds(1), 1, ThrottleMode.Shaping)
//                .Select(CountSpread)
//                .RunForeach(
//                    trade => Console.WriteLine(
//                        $"Sell {trade?.Bid?.Rate} Buy {trade?.Ask?.Rate} spread {trade?.Spread}"), mat);

            Task.Run(() =>
            {
                var hitStream = new HitOrderBookObservable(_baseHit, _quoteHit, TimeSpan.FromSeconds(0.5));
                var bitStream = new BittrexOrderBookObservable(_baseBit, _quoteBit, TimeSpan.FromSeconds(0.5));
                var qq = hitStream
                    .Zip(bitStream, (hit, bit) => new OrderBooks(hit.Asks, hit.Bids, bit.Asks, bit.Bids))
                    .Select(CountSpread)
                    .Subscribe(
                        t => Console.WriteLine($"Sell {t?.Bid?.Rate} Buy {t?.Ask?.Rate} spread {t?.Spread}"),
                        //t => Console.WriteLine("qq ppl"),
                        Console.WriteLine,
                        () => Console.WriteLine("qq"));
                hitStream.StartStream();
                bitStream.StartStream();
            });
        }

        private IEnumerator<(Order[] bids, Order[] asks)> GetHitEnumerator() =>
            new HitStreamEnumerator(_baseHit, _quoteHit);

        private IEnumerator<(Order[] bids, Order[] asks)> GetBitEnumerator() =>
            new BittrexStreamEnumerator(_baseBit, _quoteBit);

        private static Trade CountSpread(OrderBooks msg)
        {
            try
            {
                var bitBestBid = msg.BittBid[0];
                var hitBestAsk = msg.HitAsk[0];
                var bitBidHitAskSpread =
                    Math.Round(bitBestBid.Rate / hitBestAsk.Rate * 100, 1, MidpointRounding.AwayFromZero);

                var hitBestBid = msg.HitBid[0];
                var bitBestAsk = msg.BittAsk[0];
                var hitBidBitAskSpread =
                    Math.Round(hitBestBid.Rate / bitBestAsk.Rate * 100, 1, MidpointRounding.AwayFromZero);

                var bestTrade = bitBidHitAskSpread > hitBidBitAskSpread
                    ? new Trade(bitBestBid, hitBestAsk, bitBidHitAskSpread)
                    : new Trade(hitBestBid, bitBestAsk, hitBidBitAskSpread);
                return bestTrade;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new Trade();
            }
        }

        public void OnSubscribe(ISubscription subscription)
        {
        }

        public void OnNext(Trade trade) =>
            Console.WriteLine($"Sell {trade?.Bid?.Rate} Buy {trade?.Ask?.Rate} spread {trade?.Spread}");

        public void OnError(Exception cause)
        {
        }

        public void OnComplete()
        {
        }
    }

    public class HitStreamEnumerator : IEnumerator<(Order[] bids, Order[] asks)>
    {
        private readonly string _baseHit;
        private readonly string _quoteHit;
        private string hitPair => $"{_baseHit}_{_quoteHit}";

        private readonly object _lockAsk = new object();
        private readonly object _lockBid = new object();

        private readonly SortedDictionary<decimal, Order> _hitAsks =
            new SortedDictionary<decimal, Order>();

        private readonly SortedDictionary<decimal, Order> _hitBids =
            new SortedDictionary<decimal, Order>(new DescendingComparer<decimal>());


        private string HitBtcOrderBookSubscribeMsg =>
            $"{{\"method\": \"subscribeOrderbook\",\"params\": {{\"symbol\": \"{_baseHit}{_quoteHit}\"}},\"id\": 123}}";

        private ClientWebSocket _clientWebSocket;
        private CancellationTokenSource _cts;

        public HitStreamEnumerator(string baseHit, string quoteHit)
        {
            _baseHit = baseHit;
            _quoteHit = quoteHit;
            StartStreamingHitBtc();
        }

        public bool MoveNext() => true;

        public void Reset()
        {
        }

        public (Order[] bids, Order[] asks) Current
        {
            get
            {
                Order[] hitBtcAsk;
                lock (_lockAsk)
                {
                    hitBtcAsk = _hitAsks.Values.Take(10).ToArray();
                }

                Order[] hitBtcBid;
                lock (_lockBid)
                {
                    hitBtcBid = _hitBids.Values.Take(10).ToArray();
                }

                return (hitBtcBid, hitBtcAsk);
            }
        }

        object IEnumerator.Current => Current;

        private void StartStreamingHitBtc()
        {
            _cts = new CancellationTokenSource();
            var ct = _cts.Token;

            Task.Run(async () =>
            {
                await ConnectWebSocket(ct);
                var encoded = Encoding.UTF8.GetBytes(HitBtcOrderBookSubscribeMsg);
                var buff = new ArraySegment<byte>(encoded, 0, encoded.Length);
                await _clientWebSocket.SendAsync(buff, WebSocketMessageType.Text, true, ct);
                var buffer = new ArraySegment<byte>(new byte[8192]);
                using (var ms = new MemoryStream())
                {
                    while (!ct.IsCancellationRequested)
                    {
                        if (_clientWebSocket.State != WebSocketState.Open)
                        {
                            await ConnectWebSocket(ct);
                        }

                        ms.SetLength(0);
                        WebSocketReceiveResult result;
                        do
                        {
                            result = await _clientWebSocket.ReceiveAsync(buffer, CancellationToken.None);
                            ms.Write(buffer.Array, buffer.Offset, result.Count);
                        } while (!result.EndOfMessage);

                        if (result.MessageType != WebSocketMessageType.Text) continue;

                        var bytes = ms.ToArray();
                        var str = Encoding.UTF8.GetString(bytes);
                        var hitbtc = JObject.Parse(str);

                        try
                        {
                            var hitBtcAsk = hitbtc["params"]["ask"]
                                .Select(d => new Order(OrderType.Ask, StockType.HitBtc,
                                    (decimal) d["size"], (decimal) d["price"], hitPair)).ToArray();

                            lock (_lockAsk)
                            {
                                foreach (var order in hitBtcAsk)
                                {
                                    if (order.Qty == 0)
                                    {
                                        _hitAsks.Remove(order.Rate);
                                        continue;
                                    }

                                    _hitAsks[order.Rate] = order;
                                }
                            }

                            var hitBtcBid = hitbtc["params"]["bid"]
                                .Select(d => new Order(OrderType.Bid, StockType.HitBtc,
                                    (decimal) d["size"], (decimal) d["price"], hitPair)).ToArray();

                            lock (_lockBid)
                            {
                                foreach (var order in hitBtcBid)
                                {
                                    if (order.Qty == 0)
                                    {
                                        _hitBids.Remove(order.Rate);
                                        continue;
                                    }

                                    _hitBids[order.Rate] = order;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }

                    await _clientWebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "cancellation requested", ct);
                }
            }, ct);
        }

        private async Task ConnectWebSocket(CancellationToken ct)
        {
            _clientWebSocket?.Dispose();
            _clientWebSocket = new ClientWebSocket();
            await _clientWebSocket.ConnectAsync(new Uri("wss://api.hitbtc.com/api/2/ws"), ct);
        }

        public void Dispose()
        {
            _cts.Cancel();
            _clientWebSocket?.Dispose();
        }
    }

    public class BittrexStreamEnumerator : IEnumerator<(Order[] bids, Order[] asks)>
    {
        private readonly string _baseBit;
        private readonly string _quoteBit;
        private readonly HttpClient _clientHttp;

        private volatile Order[] _bids;
        private volatile Order[] _asks;

        private string bittrexGetOrderBookUrl =>
            $"https://bittrex.com/api/v1.1/public/getorderbook?market={_quoteBit}-{_baseBit}&type=both";

        private string bitPair => $"{_baseBit}_{_quoteBit}";

        public BittrexStreamEnumerator(string baseBit, string quoteBit)
        {
            _baseBit = baseBit;
            _quoteBit = quoteBit;
            _clientHttp = new HttpClient();

            Task.Run(async () => await CallApi());
        }

        private async Task CallApi()
        {
            while (true)
            {
                await Task.Delay(TimeSpan.FromSeconds(1));
                var resp = await _clientHttp.GetAsync(bittrexGetOrderBookUrl);
                var str = await resp.Content.ReadAsStringAsync();

                var bittrex = JObject.Parse(str);

                var asks = bittrex["result"]["sell"].Select(d =>
                        new Order(OrderType.Ask, StockType.Bittrex,
                            (decimal) d["Quantity"], (decimal) d["Rate"], bitPair))
                    .Take(10).ToArray();
                Interlocked.Exchange(ref _asks, asks);

                var bids = bittrex["result"]["buy"].Select(d =>
                        new Order(OrderType.Bid, StockType.Bittrex,
                            (decimal) d["Quantity"], (decimal) d["Rate"], bitPair))
                    .Take(10).ToArray();
                Interlocked.Exchange(ref _bids, bids);
            }
        }

        public bool MoveNext() => true;

        public void Reset()
        {
        }

        public (Order[] bids, Order[] asks) Current => (_bids, _asks);

        object IEnumerator.Current => Current;


        public void Dispose()
        {
        }
    }


    public class BittrexOrderBookObservable : IObservable<OrderBook>
    {
        private readonly Subject<OrderBook> _subject = new Subject<OrderBook>();
        private readonly string _baseBit;
        private readonly string _quoteBit;
        private readonly TimeSpan _interval;
        private readonly HttpClient _clientHttp;
        private readonly CancellationTokenSource _cts = new CancellationTokenSource();
        private Order[] _bids;
        private Order[] _asks;
        private readonly object _lockAsk = new object();
        private readonly object _lockBid = new object();

        private string bittrexGetOrderBookUrl =>
            $"https://bittrex.com/api/v1.1/public/getorderbook?market={_quoteBit}-{_baseBit}&type=both";

        private string bitPair => $"{_baseBit}_{_quoteBit}";

        public BittrexOrderBookObservable(string baseBit, string quoteBit, TimeSpan interval)
        {
            _baseBit = baseBit;
            _quoteBit = quoteBit;
            _interval = interval;
            _clientHttp = new HttpClient();
            CallApi();
        }

        private async Task CallApi()
        {
            while (!_cts.Token.IsCancellationRequested)
            {
                await Task.Delay(TimeSpan.FromSeconds(0.5));
                var resp = await _clientHttp.GetAsync(bittrexGetOrderBookUrl);
                var str = await resp.Content.ReadAsStringAsync();
                var bittrex = JObject.Parse(str);
                lock (_lockAsk)
                {
                    _asks = bittrex["result"]["sell"].Select(d =>
                        new Order(OrderType.Ask, StockType.Bittrex,
                            (decimal) d["Quantity"], (decimal) d["Rate"], bitPair)).ToArray();
                }

                lock (_lockBid)
                {
                    _bids = bittrex["result"]["buy"].Select(d =>
                        new Order(OrderType.Bid, StockType.Bittrex,
                            (decimal) d["Quantity"], (decimal) d["Rate"], bitPair)).ToArray();
                }
            }
        }

        public async Task StartStream()
        {
            while (!_cts.IsCancellationRequested)
            {
                try
                {
                    await Task.Delay(_interval);
                    Order[] asks;
                    lock (_lockAsk)
                    {
                        asks = _asks.Take(10).ToArray();
                    }

                    Order[] bids;
                    lock (_lockBid)
                    {
                        bids = _bids.Take(10).ToArray();
                    }

                    _subject.OnNext(new OrderBook(bids, asks));
                }
                catch (Exception)
                {
                }
            }

            _subject.OnCompleted();
        }

        public IDisposable Subscribe(IObserver<OrderBook> observer) => _subject.Subscribe(observer);
    }

    public class HitOrderBookObservable : IObservable<OrderBook>
    {
        private readonly Subject<OrderBook> _subject = new Subject<OrderBook>();
        private readonly TimeSpan _interval;
        private readonly string _baseHit;
        private readonly string _quoteHit;
        private string hitPair => $"{_baseHit}_{_quoteHit}";

        private readonly object _lockAsk = new object();
        private readonly object _lockBid = new object();

        private readonly SortedDictionary<decimal, Order> _hitAsks =
            new SortedDictionary<decimal, Order>();

        private readonly SortedDictionary<decimal, Order> _hitBids =
            new SortedDictionary<decimal, Order>(new DescendingComparer<decimal>());

        private string HitBtcOrderBookSubscribeMsg =>
            $"{{\"method\": \"subscribeOrderbook\",\"params\": {{\"symbol\": \"{_baseHit}{_quoteHit}\"}},\"id\": 123}}";

        private ClientWebSocket _clientWebSocket;
        private readonly CancellationTokenSource _cts;

        public HitOrderBookObservable(string baseHit, string quoteHit, TimeSpan interval)
        {
            _baseHit = baseHit;
            _quoteHit = quoteHit;
            _interval = interval;
            _cts = new CancellationTokenSource();
            StartStreamingHitBtc();
        }

        public async Task StartStream()
        {
            while (!_cts.IsCancellationRequested)
            {
                try
                {
                    await Task.Delay(_interval);
                    Order[] hitBtcAsk;
                    lock (_lockAsk)
                    {
                        hitBtcAsk = _hitAsks.Values.Take(10).ToArray();
                    }

                    Order[] hitBtcBid;
                    lock (_lockBid)
                    {
                        hitBtcBid = _hitBids.Values.Take(10).ToArray();
                    }

                    _subject.OnNext(new OrderBook(hitBtcBid, hitBtcAsk));
                }
                catch (Exception)
                {
                }
            }

            _subject.OnCompleted();
        }

        private void StartStreamingHitBtc() =>
            Task.Run(async () =>
            {
                var ct = _cts.Token;
                await ConnectWebSocket(ct);
                var encoded = Encoding.UTF8.GetBytes(HitBtcOrderBookSubscribeMsg);
                var buff = new ArraySegment<byte>(encoded, 0, encoded.Length);
                await _clientWebSocket.SendAsync(buff, WebSocketMessageType.Text, true, ct);
                var buffer = new ArraySegment<byte>(new byte[8192]);
                using (var ms = new MemoryStream())
                {
                    while (!ct.IsCancellationRequested)
                    {
                        if (_clientWebSocket.State != WebSocketState.Open)
                        {
                            await ConnectWebSocket(ct);
                        }

                        ms.SetLength(0);
                        WebSocketReceiveResult result;
                        do
                        {
                            result = await _clientWebSocket.ReceiveAsync(buffer, CancellationToken.None);
                            ms.Write(buffer.Array, buffer.Offset, result.Count);
                        } while (!result.EndOfMessage);

                        if (result.MessageType != WebSocketMessageType.Text) continue;

                        var bytes = ms.ToArray();
                        var str = Encoding.UTF8.GetString(bytes);
                        var hitbtc = JObject.Parse(str);

                        try
                        {
                            var hitBtcAsk = hitbtc["params"]["ask"]
                                .Select(d => new Order(OrderType.Ask, StockType.HitBtc,
                                    (decimal) d["size"], (decimal) d["price"], hitPair)).ToArray();
                            lock (_lockAsk)
                            {
                                foreach (var order in hitBtcAsk)
                                {
                                    if (order.Qty == 0)
                                    {
                                        _hitAsks.Remove(order.Rate);
                                        continue;
                                    }

                                    _hitAsks[order.Rate] = order;
                                }
                            }

                            var hitBtcBid = hitbtc["params"]["bid"]
                                .Select(d => new Order(OrderType.Bid, StockType.HitBtc,
                                    (decimal) d["size"], (decimal) d["price"], hitPair)).ToArray();
                            lock (_lockBid)
                            {
                                foreach (var order in hitBtcBid)
                                {
                                    if (order.Qty == 0)
                                    {
                                        _hitBids.Remove(order.Rate);
                                        continue;
                                    }

                                    _hitBids[order.Rate] = order;
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }

                    await _clientWebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "cancellation requested", ct);
                }
            }, _cts.Token);


        private async Task ConnectWebSocket(CancellationToken ct)
        {
            _clientWebSocket?.Dispose();
            _clientWebSocket = new ClientWebSocket();
            await _clientWebSocket.ConnectAsync(new Uri("wss://api.hitbtc.com/api/2/ws"), ct);
        }

        public IDisposable Subscribe(IObserver<OrderBook> observer)
        {
            return _subject.Subscribe(observer);
        }
    }

    public class Publisher : ActorPublisher<long>
    {
        protected override bool Receive(object message) => message.Match()
            .With<Request>(request =>
            {
                Console.WriteLine($" qq {request.Count}");
                OnNext(request.Count);
            })
            .With<Cancel>(cancel => OnCompleteThenStop())
            .WasHandled;
    }


    public class RxStream
    {
        public RxStream()
        {
            var mm = new Subject<int>();
            var s1 = mm.Skip(1);
            var delta = mm.Zip(s1,
                (prev, curr) => (prev, curr));
            delta.Subscribe(
                msg => Console.WriteLine(msg),
                () => Console.WriteLine("Completed"));
            mm.OnNext(1);
            mm.OnNext(2); //Move across 1
            mm.OnNext(3); //Diagonally up 2
            mm.OnNext(4); //Back to 0,0
            mm.OnCompleted();
        }
    }
}