﻿using alica.Data.CQRS;
using Akka.Actor;

namespace alica.Core
{
    public class WriteToDbCameo : ReceiveActor
    {
        public WriteToDbCameo()
        {
         
            Receive<(decimal lot, decimal buyRate, decimal sellRate, decimal bitBaseWallet,
                decimal bitQuoteWallet, decimal hitBaseWallet, decimal hitQuoteWallet, string currencyPair)>(msg =>
            {
                try
                {
                    using (var cmd = new WriteTradeToDbCommand())
                    {
                        (decimal lot,
                            decimal buyRate,
                            decimal sellRate,
                            decimal bitBaseWallet,
                            decimal bitQuoteWallet,
                            decimal hitBaseWallet,
                            decimal hitQuoteWallet,
                            string currencyPair) = msg;
                        cmd.Execute(lot, buyRate, sellRate, bitBaseWallet, bitQuoteWallet,
                            hitBaseWallet, hitQuoteWallet, currencyPair);
                    }
                }
                finally
                {
                    Context.Stop(Self);
                }
            });
        }
    }
}