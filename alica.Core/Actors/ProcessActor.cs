﻿using System;
using alica.Data.Models;
using Akka.Actor;
using Akka.Actor.Dsl;
using Akka.Util.Internal;
using Serilog;

namespace alica.Core
{
    public class ProcessActor : ReceiveActor
    {
        private readonly IActorRef _orderingActor;
        private decimal _deltaPercent = 101.6m;
        private readonly ILogger _logger;
        private OrderBooks _currentOrderBooks;

        public ProcessActor(IActorRef orderingActor)
        {
            _orderingActor = orderingActor;
            _logger = Log.ForContext<ProcessActor>();
            Receive<ChangeTradingMode>(msg => ChangeMode(msg));
            Become(Inactive);
        }

        private void ChangeMode(ChangeTradingMode tradingMode)
        {
            if (tradingMode.Mode == TradingMode.Trade)
            {
                Become(Active);
                _deltaPercent = tradingMode.DeltaPercent;
            }
            else
            {
                Become(Inactive);
            }
        }

        private void Active()
        {
            Receive<AskCurrentSpread>(msg => CountAskedSpread(msg));
            Receive<OrderBooks>(msg =>
            {
                try
                {
                    _currentOrderBooks = msg;
                    var bestTrade = CountSpread(msg);
                    Context.ActorSelection("/user/spreadpublisher").Tell(bestTrade);

                    if (bestTrade.Spread < _deltaPercent) return;
                    _orderingActor.Tell(bestTrade);
                }
                catch (Exception e)
                {
                    _logger.Error(e.ToString());
                }
            });
        }

        private void Inactive()
        {
            Receive<AskCurrentSpread>(msg => CountAskedSpread(msg));

            Receive<OrderBooks>(msg =>
            {
                try
                {
                    _currentOrderBooks = msg;
                    var bestTrade = CountSpread(msg);
                    Context.ActorSelection("/user/spreadpublisher").Tell(bestTrade);
                }
                catch (Exception e)
                {
                    _logger.Error(e.ToString());
                }
            });
        }

        private void CountAskedSpread(AskCurrentSpread msg)
        {
            switch (msg.StockType)
            {
                case StockType.HitBtc when msg.SideType == SideType.Sell:
                    Sender.Tell(
                        (Math.Round(_currentOrderBooks.HitBid[0].Rate / msg.Rate * 100, 1, MidpointRounding.AwayFromZero)
                        , _currentOrderBooks.HitBid[0].Rate));
                    break;
                case StockType.HitBtc when msg.SideType == SideType.Buy:
                    Sender.Tell(( Math.Round(msg.Rate / _currentOrderBooks.HitAsk[0].Rate * 100, 1,
                        MidpointRounding.AwayFromZero), _currentOrderBooks.HitAsk[0].Rate));
                    break;
                case StockType.Bittrex when msg.SideType == SideType.Sell:
                    Sender.Tell((Math.Round(_currentOrderBooks.BittBid[0].Rate / msg.Rate * 100, 1,
                        MidpointRounding.AwayFromZero), _currentOrderBooks.BittBid[0].Rate));
                    break;
                case StockType.Bittrex when msg.SideType == SideType.Buy:
                    Sender.Tell((Math.Round(msg.Rate / _currentOrderBooks.BittAsk[0].Rate * 100, 1,
                        MidpointRounding.AwayFromZero), _currentOrderBooks.BittAsk[0].Rate));
                    break;
            }
        }

        private static Trade CountSpread(OrderBooks msg)
        {
            var bitBestBid = msg.BittBid[0];
            var hitBestAsk = msg.HitAsk[0];
            var bitBidHitAskSpread =
                Math.Round(bitBestBid.Rate / hitBestAsk.Rate * 100, 1, MidpointRounding.AwayFromZero);

            var hitBestBid = msg.HitBid[0];
            var bitBestAsk = msg.BittAsk[0];
            var hitBidBitAskSpread =
                Math.Round(hitBestBid.Rate / bitBestAsk.Rate * 100, 1, MidpointRounding.AwayFromZero);

            var bestTrade = bitBidHitAskSpread > hitBidBitAskSpread
                ? new Trade(bitBestBid, hitBestAsk, bitBidHitAskSpread)
                : new Trade(hitBestBid, bitBestAsk, hitBidBitAskSpread);
            return bestTrade;
        }
    }
}