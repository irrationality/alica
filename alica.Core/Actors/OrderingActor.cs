﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alica.Core.CQRS.Bittrex;
using alica.Core.CQRS.HitBtc;
using alica.Core.CQRS.Interfaces;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Akka.Actor;
using Serilog;

namespace alica.Core
{
    public class OrderingActor : ReceiveActor
    {
        private const decimal tresholdSpread = 101;
        private string pairs => $"hitbase_{_baseHit}_hitQuote_{_quoteHit}_bitBase_{_baseBit}_bitQuote_{_quoteBit}";

        private readonly string _baseBit;
        private readonly string _baseHit;
        private readonly string _quoteBit;
        private readonly string _quoteHit;

        private Dictionary<StockType, (decimal BaseWallet, decimal QuoteWallet)> _wallets;
        private Dictionary<string, decimal> _currenciesAmount;

        private readonly decimal _tickSize;
        private decimal _minBaseLot;

        private readonly BitBalanceQuery _bitBalanceQuery = AlicaServiceLocator.GetService<BitBalanceQuery>();
        private readonly HitBalanceQuery _hitBalanceQuery = AlicaServiceLocator.GetService<HitBalanceQuery>();

        private readonly BitMinBaseLotQuery _bitMinBaseLotQuery = AlicaServiceLocator.GetService<BitMinBaseLotQuery>();
        private readonly HitMinBaseLotQuery _hitMinBaseLotQuery = AlicaServiceLocator.GetService<HitMinBaseLotQuery>();

        private readonly BitOrderCommand _bitOrderCommand = AlicaServiceLocator.GetService<BitOrderCommand>();
        private readonly HitOrderCommand _hitOrderCommand = AlicaServiceLocator.GetService<HitOrderCommand>();

        private readonly BitWithdrawCommand _bitWithdrawCommand = AlicaServiceLocator.GetService<BitWithdrawCommand>();
        private readonly HitWithdrawCommand _hitWithdrawCommand = AlicaServiceLocator.GetService<HitWithdrawCommand>();

        private readonly BitFilledOrderQuery _bitFilledOrderQuery =
            AlicaServiceLocator.GetService<BitFilledOrderQuery>();

        private readonly HitFilledOrderQuery _hitFilledOrderQuery =
            AlicaServiceLocator.GetService<HitFilledOrderQuery>();

        private readonly ILogger _logger = Log.ForContext<OrderingActor>();

        public OrderingActor(string baseHit, string quoteHit, string baseBit, string quoteBit, decimal tickSize)
        {
            _baseHit = baseHit;
            _quoteHit = quoteHit;
            _baseBit = baseBit;
            _quoteBit = quoteBit;
            _tickSize = tickSize;
            Become(Active);
        }

        protected override void PreStart()
        {
            GetWalletsAsync().Wait();
            var hitMinBaseLot = _hitMinBaseLotQuery.ExecuteAsync($"{_baseHit}{_quoteHit}").Result;
            var bitMinBaseLot = _bitMinBaseLotQuery.ExecuteAsync(_baseBit, _quoteBit).Result;
            _minBaseLot = Math.Max(hitMinBaseLot, bitMinBaseLot);
        }

        private void Active()
        {
            Receive<Trade>(msg => InvokeTrade(msg));
        }

        private void Inactive()
        {
            Receive<Trade>(trade =>
                _logger.Information("Caught spread ask {askRate}  bid {bidRate} spread {spread}  pair {pairs}",
                    trade.Ask.Rate, trade.Bid.Rate, trade.Spread, pairs));

            Receive<string>(msg =>
            {
                //todo get wallet  here
                if (msg == "qq")
                    Become(Active);
            });
        }

        private async Task GetWalletsAsync()
        {
            _wallets = new Dictionary<StockType, (decimal BaseWallet, decimal QuoteWallet)>();
            _currenciesAmount = new Dictionary<string, decimal>();

            var hitBalances = await _hitBalanceQuery.ExecuteAsync();
            var bitBaseBalance = await _bitBalanceQuery.ExecuteAsync(_baseBit);
            var bitQuoteBalance = await _bitBalanceQuery.ExecuteAsync(_quoteBit);

            _wallets.Add(StockType.HitBtc,
                (hitBalances.FirstOrDefault(x =>
                     string.Equals(x.Currency, _baseHit, StringComparison.InvariantCultureIgnoreCase))?.Available ?? 0,
                hitBalances.FirstOrDefault(x =>
                    string.Equals(x.Currency, _quoteHit, StringComparison.InvariantCultureIgnoreCase))?.Available ??
                0));
            _wallets.Add(StockType.Bittrex, (bitBaseBalance.Available, bitQuoteBalance.Available));

            _currenciesAmount[_baseBit] = _wallets[StockType.Bittrex].BaseWallet;
            _currenciesAmount[_quoteBit] = _wallets[StockType.Bittrex].QuoteWallet;
            _currenciesAmount[_baseHit] = _wallets[StockType.HitBtc].BaseWallet;
            _currenciesAmount[_quoteHit] = _wallets[StockType.HitBtc].QuoteWallet;
        }

        private async Task CheckBalanceAsync(string toSymbol, decimal amount)
        {
            while (true)
            {
                await Task.Delay(TimeSpan.FromMinutes(3));
                await GetWalletsAsync();
                if (_currenciesAmount[toSymbol] < amount)
                {
                    continue;
                }

                Become(Active);
                break;
            }
        }

        private void TransferBalance(StockType from, string fromSymbol, string toSymbol)
        {
            _logger.Information("Transfering balance from {from} for {symbol}", from, fromSymbol);
            var amount = _currenciesAmount[fromSymbol] / 2;
            if (from == StockType.HitBtc)
            {
                _hitWithdrawCommand.ExecuteAsync(fromSymbol, amount);
            }
            else
            {
                _bitWithdrawCommand.ExecuteAsync(fromSymbol, amount);
            }

            Become(Inactive);
            CheckBalanceAsync(toSymbol, amount);
        }

        private void InvokeTrade(Trade trade)
        {
            try
            {
                _logger.Information("Caught spread ask {askRate}  bid {bidRate} spread {spread}  pair {pairs}",
                    trade.Ask.Rate, trade.Bid.Rate, trade.Spread, pairs);

                var buyStock = trade.Ask.Stock;
                var sellStock = trade.Bid.Stock;

                if (_wallets[sellStock].BaseWallet < _minBaseLot)
                {
                    var fromSymbol = buyStock == StockType.HitBtc ? _baseHit : _baseBit;
                    var toSymbol = fromSymbol == _baseHit ? _baseBit : _baseHit;
                    TransferBalance(buyStock, fromSymbol, toSymbol);
                    return;
                }

                var lot = Math.Min(trade.Bid.Qty, trade.Ask.Qty);
                lot = Math.Min(lot, _wallets[sellStock].BaseWallet);
                _logger.Information("Log value is {lot}", lot);
                //lot =_minBaseLot;
                lot = 50;

                if (_wallets[buyStock].QuoteWallet < lot * trade.Ask.Rate)
                {
                    lot = _wallets[buyStock].QuoteWallet / trade.Ask.Rate; //may be should do rounding
                    if (lot < _minBaseLot)
                    {
                        var fromSymbol = sellStock == StockType.HitBtc ? _quoteHit : _quoteBit;
                        var toSymbol = fromSymbol == _quoteHit ? _quoteBit : _quoteHit;
                        TransferBalance(sellStock, fromSymbol, toSymbol);
                        return;
                    }
                }

                _logger.Information("Creating orders: Buy -> {buyStock} Sell -> {sellStock} Lot -> {lot}" +
                                    " Ask Rate -> {AskRate} Bid Rate -> {BidRate}",
                    buyStock, sellStock, lot, trade.Ask.Rate, trade.Bid.Rate);

                var (buyOrder, sellOrder) =
                    CreateOrdersAsync(buyStock, sellStock, lot, trade.Ask.Rate, trade.Bid.Rate).Result;
                CheckOrdersAsync(Self, Sender, buyOrder, sellOrder);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw;
            }
        }

        private async Task<(OpenOrder buyOrder, OpenOrder sellOrder)> CreateOrdersAsync(
            StockType buyStock, StockType sellStock,
            decimal lot, decimal buyRate, decimal sellRate)
        {
            Become(Inactive);
            var buyOrderAsync = CreateOrderAsync(buyStock, lot, buyRate, SideType.Buy);
            var sellOrderAsync = CreateOrderAsync(sellStock, lot, sellRate, SideType.Sell);

            var buyOrder = await buyOrderAsync;
            var sellOrder = await sellOrderAsync;

            _logger.Information("Buy Order: {buyOrder}", buyOrder);
            _logger.Information("Sell Order: {sellOrder}", sellOrder);
            return ( buyOrder, sellOrder);
        }

        private async Task<OpenOrder> CreateOrderAsync(StockType stock, decimal lot, decimal rate, SideType sideType)
        {
            var price = sideType == SideType.Buy ? rate + _tickSize : rate - _tickSize;

            var orderId = stock == StockType.HitBtc
                ? await _hitOrderCommand.CreateAsync(_baseHit, _quoteHit, lot, price, sideType)
                : await _bitOrderCommand.CreateAsync(_baseBit, _quoteBit, lot, price, sideType);

            var order = new OpenOrder
            {
                Qty = lot,
                Rate = price,
                Id = orderId,
                StockType = stock,
                SideType = sideType
            };
            return order;
        }

        private async Task CheckOrdersAsync(ICanTell self, ICanTell sender, OpenOrder buyOrder, OpenOrder sellOrder)
        {
            try
            {
                var checkBuyTask = CheckOrderAsync(sender, buyOrder, sellOrder);
                var checkSellTask = CheckOrderAsync(sender, sellOrder, buyOrder);

                await checkBuyTask;
                await checkSellTask;
                self.Tell("qq", ActorRefs.NoSender);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private async Task CheckOrderAsync(ICanTell sender, OpenOrder order, OpenOrder oppositeOrder)
        {
            try
            {
                IFilledOrderQuery filledOrderQuery = order.StockType == StockType.HitBtc
                    ? (IFilledOrderQuery) _hitFilledOrderQuery
                    : _bitFilledOrderQuery;

                var savedRemainQty = 0m;
                while (true)
                {
                    await Task.Delay(TimeSpan.FromSeconds(10));
                    var remainQty = await filledOrderQuery.ExecuteAsync(order.Id);

                    if (remainQty == 0)
                        return;

                    if (remainQty == savedRemainQty)
                        break;
                    savedRemainQty = remainQty;
                }

                var cancelTask = order.StockType == StockType.HitBtc
                    ? _hitOrderCommand.CancelAsync(order.Id)
                    : _bitOrderCommand.CancelAsync(order.Id);

                var (currentSpread, rate) = await sender.Ask<(decimal currentSpread, decimal rate)>
                    (new AskCurrentSpread(order.StockType, oppositeOrder.Rate, order.SideType));

                if (currentSpread >= tresholdSpread)
                {
                    order = await CreateOrderAsync(order.StockType, savedRemainQty, rate, order.SideType);
                    await CheckOrderAsync(sender, order, oppositeOrder);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}