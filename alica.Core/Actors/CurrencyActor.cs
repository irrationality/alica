﻿using System;
using System.Threading.Tasks;
using alica.Data.CQRS;
using alica.Data.Infrastructure;
using alica.Data.Models;
using Akka.Actor;
using Microsoft.Extensions.Options;

namespace alica.Core
{
    public class CurrencyActor : ReceiveActor
    {
        private readonly string _baseHit;
        private readonly string _quoteHit;
        private readonly string _baseBit;
        private readonly string _quoteBit;
        private readonly decimal _tickSize;


        private IActorRef _orderActor;
        private IActorRef _streamActor;
        private IActorRef _processActor;
        

        public CurrencyActor(string baseHit, string quoteHit, string baseBit, string quoteBit, decimal tickSize)
        {
            _baseHit = baseHit;
            _quoteHit = quoteHit;
            _baseBit = baseBit;
            _quoteBit = quoteBit;
            _tickSize = tickSize;

            Receive<CallBittrex>(msg => 
                _streamActor.Tell(msg));
        }
        

        protected override void PreStart()
        {
            
            _orderActor = Context.ActorOf(Props.Create(() => new OrderingActor(_baseHit,_quoteHit,_baseBit,_quoteBit, _tickSize)));
            _processActor = Context.ActorOf(Props.Create(() => new ProcessActor(_orderActor)));
            _streamActor = Context.ActorOf(Props.Create(() => 
                new StreamActor(_baseHit,_quoteHit,_baseBit,_quoteBit,_processActor)));
            
            Task.Run(() =>
            {
                using (var qq = AlicaServiceLocator.GetService<CreateOrUpdatePairRecordCommand>() )
                {
                    qq.Execute(_baseHit,_quoteHit,_baseBit,_quoteBit,_tickSize);   
                }
            });
        }

        protected override SupervisorStrategy SupervisorStrategy() => new OneForOneStrategy(
            10,
            TimeSpan.FromMinutes(1),
            ex => Directive.Restart);
    }

    
}